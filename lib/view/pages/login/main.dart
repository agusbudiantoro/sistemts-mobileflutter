import 'package:sistemts/model/model_login/model_form_login.dart';
import 'package:sistemts/view/pages/admin/home_admin.dart';
import 'package:sistemts/view/pages/register/main.dart';
import 'package:sistemts/view/pages/users/home_users.dart';
import 'package:sistemts/view/util/color.dart';
import 'package:sistemts/view/util/login/login/custom_button.dart';
import 'package:sistemts/view/util/login/login/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sistemts/view_model/bloc/bloc_login/login_bloc.dart';

class LoginMain extends StatefulWidget {
  const LoginMain({ Key? key }) : super(key: key);

  @override
  _LoginMainState createState() => _LoginMainState();
}

class _LoginMainState extends State<LoginMain> {
  final TextEditingController _email = TextEditingController();
  final TextEditingController _passWord = TextEditingController();
  LoginBloc bloc = LoginBloc();
  bool statusPass = true;
  void _clickCallBack() {
    setState(() {
      statusPass = !statusPass;
    });
  }

  @override
  void initState() {
    super.initState();
    // bloc..add(BlocEventDisconnect());
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: blackBackground,
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        child: Container(
          height: size.height,
          decoration: BoxDecoration(
            // color: background1.withOpacity(0.7),
              // image: DecorationImage(
              //   image: AssetImage("assets/login/login.jpg"),
              //   fit: BoxFit.cover,
              // ),
            ),
          child: Column(
          children: [
            Container(
              color: redColorBack,
              height: size.height-(size.height/1.4),
              child: Stack(
                children: [
                  Container(
                    color: Colors.transparent,
                    width: size.width,
                    child: new Image.asset('assets/logo/logo.png'),
                  ),
                ],
              ),
            ),
            Container(
              height: size.height/1.4,
              color: blackBackground,
             
              child: Stack(
                children: [
                  Container(
                    color: Colors.transparent,
                  ),
                  Container(
                    // padding:const EdgeInsets.symmetric(horizontal: 24, vertical: 28),
                    decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius:
                            BorderRadius.only(topRight: Radius.circular(88))),
                            child: Container(
                              padding:const EdgeInsets.only(top:10),
                        alignment: Alignment.center,
                        child: Column(
                            children: [
                              // Image.asset("assets/logo/logo.png", height: 200,),
                              Text(
                                "Masuk",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 30.0,
                                    fontWeight: FontWeight.bold,fontFamily: 'RobotoCondensed'),
                              ),
                              SizedBox(height:10),
                              // Spacer(),
                              TextFieldLogin(
                                typePass: false,
                                typeInput: false,
                                isiField: _email,
                                hintText: "Email",
                                prefixIcon: Icons.email,
                              ),
                              TextFieldLogin(
                                typePass: true,
                                typeInput: false,
                                  isiField: _passWord,
                                  hintText: "Password",
                                  prefixIcon: Icons.vpn_key_rounded,
                                  isObsercure: statusPass,
                                  suffixIcon: Icons.remove_red_eye,
                                  clickCallback: () => _clickCallBack()),
                              SizedBox(
                                height: 10.0,
                              ),
                              Container(
                                width: size.width,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    BlocListener<LoginBloc, LoginState>(
                                    bloc: bloc,
                                    listener: (context, state) {
                                      if(state is BlocStateSukses){
                                        ScaffoldMessenger.of(context).showSnackBar(
                                          SnackBar(
                                            content: Text("Login Berhasil"),
                                            duration: Duration(milliseconds: 500),
                                            backgroundColor: Colors.green,
                                            onVisible: (){
                                              Navigator.pop(context);
                                              if(state.myData!.role == 2){
                                                Navigator.push(context, MaterialPageRoute(builder: (context)=>BackgroundHome()));
                                              }else {
                                                Navigator.push(context, MaterialPageRoute(builder: (context)=>BackgroundHomeAdmin()));
                                              }
                                              
                                            },
                                          )
                                        );
                                      }
                                      if(state is BlocaStateFailed){
                                        ScaffoldMessenger.of(context).showSnackBar(
                                          SnackBar(
                                            content: Text("Login Gagal, "+state.errorMessage.toString()),
                                            duration: Duration(milliseconds: 500),
                                            backgroundColor: Colors.red,
                                          )
                                        );
                                      }
                                    },
                                    child: Container(
                                      child: BlocBuilder<LoginBloc, LoginState>(
                                        bloc: bloc,
                                        builder: (context, state) {
                                          if (state is BlocStateSukses) {
                                            return buildButtonLogin();
                                          }
                                          if (state is BlocStateLoading) {
                                            return Center(
                                              child: CircularProgressIndicator(color: Colors.white,),
                                            );
                                          }
                                          if (state is BlocaStateFailed) {
                                            return buildButtonLogin();
                                          }
                                          if (state is BlocLoginInitial) {
                                            return buildButtonLogin();
                                          }
                                          return buildButtonLogin();
                                        },
                                      ),
                                    ),
                                  ),
                                  ButtonLogin(
                                    onPress: (){
                                      print("coab");
                                      // Navigator.push(context, MaterialPageRoute(builder: (context)=>PageChat(id:2)));
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=>const RegisterMain()));
                                    },
                                    buttonName: "Daftar",
                                    paddingH: 35.0,
                                  )
                                  ],
                                ),
                              ),
                              Spacer()
                            ],
                          ),
                      ),
                  )
                ],
              ),
            )
          ],
            ),
        ),
      ),
    );
  }

  ButtonLogin buildButtonLogin() {
    return ButtonLogin(
      onPress: (){
        print("sign");
        ModelFormLogin isi = ModelFormLogin(email: _email.text.toString(), password: _passWord.text.toString());
        bloc..add(BlocEventLogin(myData: isi));
      },
      buttonName: "Masuk",
      paddingH: 35.0,
    );
  }
}