import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sistemts/model/model_jadwal/model_list_jadwal.dart';
import 'package:sistemts/view/util/color.dart';
import 'package:sistemts/view_model/bloc/bloc_jadwal/bloc_jadwal_bloc.dart';

class PageJadwal extends StatefulWidget {
  const PageJadwal({ Key? key }) : super(key: key);

  @override
  _PageJadwalState createState() => _PageJadwalState();
}

class _PageJadwalState extends State<PageJadwal> {
  BlocJadwalBloc bloc = BlocJadwalBloc();
  int? jumlahJadwal;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc..add(EventGetJadwal());
  }

  @override
  Widget build(BuildContext context) {
    var size= MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: blackBackground,
      body: Container(
        color: Colors.transparent,
        margin: EdgeInsets.all(20),
      child: Column(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.only(top:size.height/16),
              color: Colors.transparent,
              child: Row(
                children: [
                  Expanded(
                    flex: 3,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Text("Daftar", style: TextStyle(color: captionColor2, fontSize: 30, fontWeight: FontWeight.normal),)),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Text("Jadwal", style: TextStyle(color: captionColor2, fontSize: 30, fontWeight: FontWeight.bold),))
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.topCenter,
                          height: size.height/10,
                          decoration: BoxDecoration(
                          border: Border.all(color: redColorBack, width: 3),
                            color: Colors.transparent, 
                            borderRadius: BorderRadius.circular(20)),
                            child: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(jumlahJadwal.toString(), style: TextStyle(color: captionColor2, fontSize: 20, fontWeight: FontWeight.normal)),
                                  Text("Jadwal", style: TextStyle(color: captionColor2, fontSize: 20, fontWeight: FontWeight.normal)),
                                ],
                              ),
                            ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 4,
            child: Container(
              width: size.width,
              decoration: BoxDecoration(
                            color: Colors.transparent, 
                            borderRadius: BorderRadius.circular(20)),
              child: BlocListener<BlocJadwalBloc, BlocJadwalState>(
                bloc: bloc,
                listener: (context, state) {
                  // TODO: implement listener
                  if(state is BlocGetJadwalSuccessState){
                    setState(() {
                      jumlahJadwal=state.listData!.length;
                    });
                  }
                },
                child: Container(
                  child: BlocBuilder<BlocJadwalBloc, BlocJadwalState>(
                bloc: bloc,
                builder: (context, state) {
                  if(state is BlocGetJadwalSuccessState){
                    return ListViewChild(size, state.listData);
                  }
                  if(state is BlocGetJadwalLoadingState){
                    return Container(child: Center(child: CircularProgressIndicator(),),);
                  }
                  if(state is BlocGetJadwalFailedState){
                    return Container(child: Center(child: Text("Tidak dapat menampilkan data"),),);
                  }
                  return Container();
                },
              ),
                ),
              )
            ),
          )
        ],
      ),
    ),
    );
  }

  ListView ListViewChild(Size size, List<ModelListJadwal>? data) {
    return ListView.builder(
              shrinkWrap: true,
              physics: AlwaysScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              itemCount: data!.length,
              itemBuilder: (BuildContext context, int i){
                return Container(
                  margin: EdgeInsets.all(5),
                  height: size.height/10,
                  width: size.width,
                  decoration: BoxDecoration(
                        border: Border.all(color: blackMetalic, width: 2),
                          color: blackMetalic, 
                          borderRadius: BorderRadius.circular(20)),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 4,
                                child: Container(
                                  margin: EdgeInsets.all(15),
                                  // color: Colors.transparent,
                                  child: Column(
                                    children: [
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text("Waktu : "+data[i].waktu.toString(), style: TextStyle(color: captionColor2, fontSize: 14, fontWeight: FontWeight.normal))),
                                        )
                                    ],
                                  ),
                                )),
                                Expanded(
                                flex: 1,
                                child: Container(
                                  decoration: BoxDecoration(
                                  // border: Border.all(color: blackMetalic, width: 2),
                                    color: redColorBack, 
                                    borderRadius: BorderRadius.only(topRight: Radius.circular(20),bottomRight: Radius.circular(20))),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(data[i].tempat!, style: TextStyle(color: captionColor2, fontSize: 20, fontWeight: FontWeight.normal))
                                    ],
                                  )
                                ))
                            ],
                          ),
                );
              }
              );
  }
}