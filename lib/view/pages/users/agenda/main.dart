import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sistemts/model/model_list_bidang/model_list_bidang.dart';
import 'package:sistemts/view/pages/users/agenda/list_agenda/main.dart';
import 'package:sistemts/view/util/color.dart';
import 'package:sistemts/view_model/bloc/bloc_bidang/bloc_bidang_bloc.dart';

class Agenda extends StatefulWidget {
  const Agenda({ Key? key }) : super(key: key);

  @override
  _AgendaState createState() => _AgendaState();
}

class _AgendaState extends State<Agenda> {
  BlocBidangBloc bloc = BlocBidangBloc();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc..add(EventGetBidang());
  }
  
  @override
  Widget build(BuildContext context) {
    var size= MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: blackBackground,
      body: Container(
        color: Colors.transparent,
        margin: EdgeInsets.all(20),
      child: Column(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.only(top:size.height/16),
              color: Colors.transparent,
              child: Row(
                children: [
                  Expanded(
                    flex: 3,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Text("Daftar", style: TextStyle(color: captionColor2, fontSize: 30, fontWeight: FontWeight.normal),)),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Text("Bidang", style: TextStyle(color: captionColor2, fontSize: 30, fontWeight: FontWeight.bold),))
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.topCenter,
                          height: size.height/10,
                          decoration: BoxDecoration(
                          border: Border.all(color: redColorBack, width: 3),
                            color: Colors.transparent, 
                            borderRadius: BorderRadius.circular(20)),
                            child: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("3", style: TextStyle(color: captionColor2, fontSize: 20, fontWeight: FontWeight.normal)),
                                  Text("Bidang", style: TextStyle(color: captionColor2, fontSize: 20, fontWeight: FontWeight.normal)),
                                ],
                              ),
                            ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 4,
            child: Container(
              width: size.width,
              decoration: BoxDecoration(
                            color: Colors.transparent, 
                            borderRadius: BorderRadius.circular(20)),
              child: BlocBuilder<BlocBidangBloc, BlocBidangState>(
                bloc: bloc,
                builder: (context, state) {
                  if(state is BlocGetBidangSuccessState){
                    return ListViewChild(size, state.listData);
                  }
                  if(state is BlocGetBidangLoadingState){
                    return Container(child: Center(child: CircularProgressIndicator(),),);
                  }
                  if(state is BlocGetBidangFailedState){
                    return Container(child: Center(child: Text("Tidak dapat menampilkan data"),),);
                  }
                  return Container();
                },
              ),
            ),
          )
        ],
      ),
    ),
    );
  }

  ListView ListViewChild(Size size, List<ModelListBidang>? data) {
    return ListView.builder(
              shrinkWrap: true,
              physics: AlwaysScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              itemCount: data!.length,
              itemBuilder: (BuildContext context, int i){
                return Container(
                  margin: EdgeInsets.all(5),
                  height: size.height/5,
                  width: size.width,
                  decoration: BoxDecoration(
                        border: Border.all(color: blackMetalic, width: 2),
                          color: blackMetalic, 
                          borderRadius: BorderRadius.circular(20)),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 4,
                                child: Container(
                                  margin: EdgeInsets.all(15),
                                  // color: Colors.transparent,
                                  child: Column(
                                    children: [
                                      Expanded(
                                        flex: 2,
                                        child: Container(
                                          child: Text(data[i].namaBidang!, style: TextStyle(color: captionColor2, fontSize: 20, fontWeight: FontWeight.normal)))),
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text("Agenda Kegiatan : "+data[i].jumlahAgenda.toString(), style: TextStyle(color: captionColor2, fontSize: 14, fontWeight: FontWeight.normal))),
                                        )
                                    ],
                                  ),
                                )),
                                Expanded(
                                flex: 1,
                                child: GestureDetector(
                                  onTap: (){
                                    Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>ListAgenda(id: data[i].idBidang,)));
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                    // border: Border.all(color: blackMetalic, width: 2),
                                      color: redColorBack, 
                                      borderRadius: BorderRadius.only(topRight: Radius.circular(20))),
                                    child: Column(
                                      children: [
                                        Expanded(
                                          flex: 1,
                                          child: Icon(Icons.arrow_forward_ios, size: size.width/10,color: Colors.white,),
                                        )
                                      ],
                                    )
                                  ),
                                ))
                            ],
                          ),
                );
              }
              );
  }
}