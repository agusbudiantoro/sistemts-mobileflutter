import 'package:sistemts/model/model_register/form_register_user.dart';
import 'package:sistemts/view/pages/users/home_users.dart';
import 'package:sistemts/view/util/color.dart';
import 'package:sistemts/view/util/login/login/custom_button.dart';
import 'package:sistemts/view/util/login/login/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sistemts/view/util/register/register/custom_button.dart';
import 'package:sistemts/view/util/register/register/custom_text.dart';
import 'package:sistemts/view_model/bloc/bloc_register/register_bloc.dart';

class RegisterMain extends StatefulWidget {
  const RegisterMain({ Key? key }) : super(key: key);

  @override
  _RegisterMainState createState() => _RegisterMainState();
}

class _RegisterMainState extends State<RegisterMain> {
  final TextEditingController _email = TextEditingController();
  final TextEditingController _passWord = TextEditingController();
  final TextEditingController _nim = TextEditingController();
  final TextEditingController _nama_user = TextEditingController();
  final TextEditingController _no_hp = TextEditingController();
  final TextEditingController _tempat_lahir= TextEditingController();
  final TextEditingController _tgl_lahir= TextEditingController();
  final TextEditingController _alamat = TextEditingController();
  final TextEditingController _jenis_kelamin = TextEditingController();
  final TextEditingController _agama =TextEditingController();
  final TextEditingController _tinggi_badan =TextEditingController();
  final TextEditingController _fakultas_jurusan =TextEditingController();
  final TextEditingController _nama_ayah = TextEditingController();
  final TextEditingController _nama_ibu = TextEditingController();
  final TextEditingController _berat_badan = TextEditingController();

  RegisterBloc bloc = RegisterBloc();
  bool statusPass = true;
  void _clickCallBack() {
    setState(() {
      statusPass = !statusPass;
    });
  }

  @override
  void initState() {
    super.initState();
    // bloc..add(BlocEventDisconnect());
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: redColorBack,
      body: Container(
        height: size.height,
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          physics: AlwaysScrollableScrollPhysics(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              // height: size.height,
                              padding:const EdgeInsets.symmetric(vertical: 70),
                        alignment: Alignment.center,
                        child: Column(
                            children: [
                              // Image.asset("assets/logo/logo.png", height: 200,),
                              const Text(
                                "Register",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 30.0,
                                    fontWeight: FontWeight.bold,fontFamily: 'RobotoCondensed'),
                              ),
                              SizedBox(height:10),
                              // Spacer(),
                              TextFieldRegister(
                                typePass: false,
                                typeInput: false,
                                isiField: _email,
                                hintText: "Email",
                                prefixIcon: Icons.email,
                              ),
                              TextFieldRegister(
                                typePass: false,
                                typeInput: false,
                                isiField: _nama_user,
                                hintText: "Nama",
                                prefixIcon: Icons.person_pin,
                              ),
                              TextFieldRegister(
                                typePass: true,
                                typeInput: false,
                                  isiField: _passWord,
                                  hintText: "Password",
                                  prefixIcon: Icons.vpn_key_rounded,
                                  isObsercure: statusPass,
                                  suffixIcon: Icons.remove_red_eye,
                                  clickCallback: () => _clickCallBack()),
                              SizedBox(height: 20,),
                              Container(
                                width: size.width,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    BlocListener<RegisterBloc, RegisterState>(
                                    bloc: bloc,
                                    listener: (context, state) {
                                      if(state is BlocStateSukses){
                                        ScaffoldMessenger.of(context).showSnackBar(
                                          SnackBar(
                                            content: Text("Register Berhasil"),
                                            duration: Duration(milliseconds: 500),
                                            backgroundColor: Colors.green,
                                            onVisible: (){
                                              // Navigator.push(context, MaterialPageRoute(builder: (context)=>BackgroundHome()));
                                            },
                                          )
                                        );
                                        Future.delayed(Duration(milliseconds: 502), () => Navigator.pop(context));
                                      }
                                      if(state is BlocaStateFailed){
                                        ScaffoldMessenger.of(context).showSnackBar(
                                          SnackBar(
                                            content: Text("Register Gagal, "+state.errorMessage.toString()),
                                            duration: Duration(milliseconds: 500),
                                            backgroundColor: Colors.red,
                                          )
                                        );
                                      }
                                    },
                                    child: Container(
                                      child: BlocBuilder<RegisterBloc, RegisterState>(
                                        bloc: bloc,
                                        builder: (context, state) {
                                          if (state is BlocStateSukses) {
                                            return buildButtonRegister();
                                          }
                                          if (state is BlocStateLoading) {
                                            return Center(
                                              child: CircularProgressIndicator(color: Colors.white,),
                                            );
                                          }
                                          if (state is BlocaStateFailed) {
                                            return buildButtonRegister();
                                          }
                                          return buildButtonRegister();
                                        },
                                      ),
                                    ),
                                  ),
                                  ButtonRegister(
                                    onPress: (){
                                      registUser formData = registUser(email: _email.text, password: _passWord.text, namaUser: _nama_user.text);
                                      bloc..add(BlocEventRegister(myData: formData));
                                      // print("coab");
                                      // Navigator.push(context, MaterialPageRoute(builder: (context)=>PageChat(id:2)));
                                      // Navigator.push(context, MaterialPageRoute(builder: (context)=>ListChat()));
                                    },
                                    buttonName: "Daftar",
                                    paddingH: 35.0,
                                  )
                                  ],
                                ),
                              ),
                              // Spacer()
                            ],
                          ),
                      ),
          ],
            ),
        ),
      ),
    );
  }

  ButtonRegister buildButtonRegister() {
    return ButtonRegister(
      onPress: (){
        print("sign");
        Navigator.pop(context);
      },
      buttonName: "Kembali",
      paddingH: 35.0,
    );
  }
}