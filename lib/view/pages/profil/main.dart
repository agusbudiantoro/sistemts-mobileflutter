import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sistemts/model/model_jadwal/model_list_jadwal.dart';
import 'package:sistemts/view/pages/login/main.dart';
import 'package:sistemts/view/pages/profil/form.dart';
import 'package:sistemts/view/util/color.dart';
import 'package:sistemts/view/widgets/home/buttoncustom1.dart';
import 'package:sistemts/view_model/bloc/bloc_jadwal/bloc_jadwal_bloc.dart';

class Profil extends StatefulWidget {
  const Profil({ Key? key }) : super(key: key);

  @override
  _ProfilState createState() => _ProfilState();
}

class _ProfilState extends State<Profil> {
  BlocJadwalBloc bloc = BlocJadwalBloc();
  String? nama;
  String? email;
  int? jumlahJadwal;
  int? role;
  int? id;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc..add(EventGetJadwal());
    getData();
  }

  void getData()async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      nama = preferences.getString("myname");
      email = preferences.getString("email");
      role = preferences.getInt("role");
      id = preferences.getInt("id_pengguna");
    });
  }

  @override
  Widget build(BuildContext context) {
    var size= MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(),
      backgroundColor: blackBackground,
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        physics: AlwaysScrollableScrollPhysics(),
        child: Container(
          color: Colors.transparent,
          margin: EdgeInsets.all(20),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top:size.height/30),
              color: Colors.transparent,
              child: Column(
                children: [
                  Container(
                    // flex: 1,
                    child: Icon(Icons.person_outline_outlined, color: Colors.white,size: size.width/5,)
                  ),
                  Container(
                    // flex: 1,
                    child: Column(
                      children: [
                        Text(nama.toString(), style: TextStyle(color: Colors.white),),
                        SizedBox(height: 10,),
                        Text(email.toString(), style: TextStyle(color: Colors.white),),
                        SizedBox(height: 10,),
                        button1(name: "Keluar",collorName: captionColor2,iconButton: Icons.logout_rounded,collorIcon: blackMetalic,collorButton: Colors.transparent,borderRadius: 88,fillCollor: redColorBack, clickCallback: ()=>_clickCallBackDelete(),)
                      ],
                    )
                  ),
                ],
              ),
            ),
            Container(
              width: size.width,
              decoration: BoxDecoration(
                            color: Colors.transparent, 
                            borderRadius: BorderRadius.circular(20)),
              child: (role == 2)?ListViewChild(size):Container(),
            )
          ],
        ),
    ),
      ),
    );
  }

  _clickCallBackDelete()async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString("email", "");
      await preferences.setString("token", "");
      await preferences.setString("myname", "");
      await preferences.setInt("role", 0);
      await preferences.setInt("id_pengguna", 0);
    Navigator.pop(context);
    Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>LoginMain()));
  }

  ListView ListViewChild(Size size) {
    return ListView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              children: [
                FormMain(id:id)
              ],
              );
  }
}