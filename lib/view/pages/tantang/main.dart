import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sistemts/view/util/color.dart';
import 'package:sistemts/view_model/api/domain.dart';

class Tentang extends StatefulWidget {
  final int? id;
  Tentang({this.id});

  @override
  _TentangState createState() => _TentangState();
}

class _TentangState extends State<Tentang> {
  PageController pageController = PageController(keepPage: true);
  int currentPage=0;
  String url = urlDomain+"/controller/foto/";
  List<Map<String,String>> stepData =[
    {
      "text":"Visi",
      "isi":"MENJADIKAN UKM TAPAK SUCI UHAMKA SEBAGAI WADAH UNTUK MEMPERERAT UKHUWAH ISLAMIYAH, MENGEMBANGKAN PRESTASI, BERJIWA SOSIAL SERTA MELESTARIKAN KEBUDAYAAN TAPAK SUCI MENURUT SYARIAT ISLAM"
    },
    {
      "text":"Misi",
      "isi":"1.Mempererat ukhuwah islamiyah baik secara eksternal maupun internal \n 2. berperan aktif dalam meningkatkan dan mengembangkan kualitas atlet turut serta dalam melestarikan kebudayaan pencak silat \n 3. turut serta dalam melestarikan kebudayaan pencak silat \n 4. membangun dan mengembangkan program-program yang sudah ada sebelumnya menjadi lebih baik \n 5. Mensyiarkan Tapak Suci dan menegakan amar mar'uf nahi mungkar"
    }
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: blackBackground,
      body: Container(
        child: ParentBody(size),
      ),
    );
  }

  Column ParentBody(Size size) {
    return Column(
        children: [
          Expanded(
            flex: 5,
            child: ClipPath(
              clipper: MyClipper(),
              child: Container(
                color: Colors.red,
                child: Column(
                  children: [
                    Expanded(
                      flex: 8,
                      child: Stack(
                        children: [
                          Positioned(
                            top: size.height/7,
                            // left: 20,
                            child: Container(
                              padding: EdgeInsets.all(20),
                              height: size.height,
                                    width: size.width,
                              child: PageView.builder(
                                physics: AlwaysScrollableScrollPhysics(),
                                controller: pageController,
                                onPageChanged: (val){
                                          setState(() {
                                            currentPage=val;
                                            // if(currentPage <= 1){
                                            //   val=currentPage;
                                            // }
                                          });      
                                          print(currentPage); 
                                        },
                                itemCount: stepData.length,
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (BuildContext context, int i){
                                  return Container(
                                    
                                    child: Column(
                                      children:[
                                        Text(stepData[currentPage]['isi']!, style: TextStyle(color: Colors.white, fontSize: 20),)
                                      ]
                                    ),
                                  );
                                }
                                ),
                            ),
                          ),
                            Positioned(
                              top: size.height/10,
                            child: IconButton(
                              onPressed: (){
                                Navigator.pop(context);
                              }, 
                              icon: Icon(Icons.arrow_back_ios_new_rounded,color: Colors.white, ))
                            ),
                        ],
                      )
                      
                    ),
                      Expanded(
                        flex: 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: List.generate(stepData.length,(index) => pagination(index)),)
                        )
                  ],
                ),
                ),
            )
              ),
            Expanded(
            flex: 1,
            child: Container(
              margin: EdgeInsets.all(20),
              color: Colors.transparent,
              child: PageView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        controller: pageController,
                        onPageChanged: (val){
                                  setState(() {
                                    currentPage=val;
                                    // if(currentPage <= 1){
                                    //   val=currentPage;
                                    // }
                                  });     
                                  print(currentPage); 
                                },
                        itemCount: stepData.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (BuildContext context, int i){
                          return ContentText();
                        }
                        ),
              )),
        ],
      );
  }

  Column ContentText() => Column(
    children: [
      Text(stepData[currentPage]['text']!, style: TextStyle(color: Colors.white,fontSize: 20,),),
    ],
  );


  Container pagination(index) {
    return Container(
                  margin: EdgeInsets.only(right:5),
                  height: 6,
                  width: (currentPage==index)?20:6,
                  decoration: BoxDecoration(
                    color: (currentPage==index)?blackBackground:captionColor,
                    borderRadius: BorderRadius.circular(3)
                  ),
                );
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size){
    var path = new Path();
    path.lineTo(0, size.height/2);
    var controllPoint = Offset(size.width/8, size.height);
    var endPoint= Offset(size.width/2, size.height);
    path.quadraticBezierTo(controllPoint.dx, controllPoint.dy, endPoint.dx,endPoint.dy);
    path.lineTo(size.width, size.height);
    
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path>oldClipper){
    return true;
  }
  
}