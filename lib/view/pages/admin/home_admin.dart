import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sistemts/view/pages/admin/agenda/main.dart';
import 'package:sistemts/view/pages/admin/biodata_kejuaraan.dart/main.dart';
import 'package:sistemts/view/pages/admin/jadwal/main.dart';
import 'package:sistemts/view/pages/admin/penerimaan_kader/main.dart';
import 'package:sistemts/view/pages/admin/peserta/main.dart';
import 'package:sistemts/view/pages/profil/main.dart';
import 'package:sistemts/view/pages/tantang/main.dart';
import 'package:sistemts/view/util/color.dart';
import 'package:sistemts/view/widgets/home/list_menu.dart';
import 'package:sistemts/view_model/bloc/bloc_login/login_bloc.dart';




class BackgroundHomeAdmin extends StatefulWidget {
  @override
  _BackgroundHomeAdminState createState() => _BackgroundHomeAdminState();
}

class _BackgroundHomeAdminState extends State<BackgroundHomeAdmin> {
  bool statusPass = true;
  LoginBloc bloc = LoginBloc();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _passWord = TextEditingController();
  String? nama;
  int? status;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }

  void _clickCallBack() {
    setState(() {
      statusPass = !statusPass;
      print(statusPass);
    });
  }

  void getData()async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      nama = preferences.getString("myname");
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
          width: size.width,
          height: 900,
          color: blackBackground,
          child: Stack(
            children: [
              Container(
                height: size.height / 2,
                width: size.width,
                color: Colors.black,
                child: Padding(
                  padding: EdgeInsets.only(top: size.height / 12),
                  child: Column(
                    children: [
                      Expanded(
                        flex: 1,
                        child: const Text(
                          "Selamat Datang",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 25.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'RobotoCondensed'),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                          nama.toString(),
                          style: const TextStyle(
                              color: Colors.white,
                              fontSize: 15.0,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'RobotoCondensed'),
                        ),
                      ),
                      Expanded(
                        flex: 10,
                        child: Container(
                          child: Image.asset('assets/logo/logo.png'),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SingleChildScrollView(
                physics: const AlwaysScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                child: Container(
                    height: 1000,
                    alignment: Alignment.center,
                    child: Stack(
                      children: [
                        Positioned(
                          top: size.height / 4,
                          child: Container(
                            width: size.width,
                            height: size.height,
                            child: Container(
                              alignment: Alignment.topCenter,
                              margin: EdgeInsets.only(top:size.height/10,right: 20, left: 20),
                              child: Wrap(
                                children: [
                                  ListMenu(sizeHeight: size.height, sizeWidth: size.width,iconMenu: Icons.assignment_late,judul: "Tentang",fontJudul: 14, clickPage: Tentang(),),
                                  ListMenu(sizeHeight: size.height, sizeWidth: size.width,iconMenu: Icons.calendar_today_sharp,judul: "Jadwal",fontJudul: 14, clickPage: PageJadwalAdmin(),),
                                  ListMenu(sizeHeight: size.height, sizeWidth: size.width,iconMenu: Icons.list_alt_rounded,judul: "Biodata Kejuaraan",fontJudul: 11, clickPage: BiodataKejuaraanAdmin(),),
                                  ListMenu(sizeHeight: size.height, sizeWidth: size.width,iconMenu: Icons.book_outlined,judul: "Penerimaan Kader",fontJudul: 11, clickPage: PenerimaanKaderAdmin(),),
                                  ListMenu(sizeHeight: size.height, sizeWidth: size.width,iconMenu: Icons.featured_play_list_rounded,judul: "Agenda",fontJudul: 11, clickPage: AgendaAdmin(),),
                                  ListMenu(sizeHeight: size.height, sizeWidth: size.width,iconMenu: Icons.people,judul: "Peserta",fontJudul: 11, clickPage: PagePesertaAdmin(),),
                                  ListMenu(sizeHeight: size.height, sizeWidth: size.width,iconMenu: Icons.assignment_ind_rounded,judul: "Profile",fontJudul: 14, clickPage: Profil(),),
                                ],
                              )
                              ),
                            ),
                          ),
                      ],
                    )),
              ),
            ],
          )),
    );
  }

  // ButtonLogin buildButtonLogin() {
  //   return ButtonLogin(
  //     onPress: () {
  //       MyDataFormLogin isi = MyDataFormLogin(
  //           email: _email.text.toString(), password: _passWord.text.toString());
  //       bloc..add(BlocEventLogin(myData: isi));
  //     },
  //     buttonName: "Masuk",
  //     paddingH: 35.0,
  //   );
  // }
}
