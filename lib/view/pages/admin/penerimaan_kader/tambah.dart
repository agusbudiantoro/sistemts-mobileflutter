// import 'dart:io';

// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:sistemts/model/model_biodata_kejuaraan/model_form_biodata_kejuaraan.dart';
// import 'package:sistemts/model/model_penerimaan_kader/model_form.dart';
// import 'package:sistemts/view/util/color.dart';
// import 'package:image_picker/image_picker.dart';
// import 'package:file_picker/file_picker.dart';
// import 'package:sistemts/view_model/bloc/bloc_biodata_kejuaraan/biodata_kejuaraan_bloc.dart';
// import 'package:sistemts/view_model/bloc/bloc_penerimaan_kader/bloc_penerimaan_kader_bloc.dart';

// typedef TambahData = void Function();
// class PageTambah extends StatefulWidget {
//   final TambahData? callback;
//   PageTambah({this.callback});

//   @override
//   _PageTambahState createState() => _PageTambahState();
// }

// class _PageTambahState extends State<PageTambah> {
//   BlocPenerimaanKaderBloc blocBio = BlocPenerimaanKaderBloc();
//   final ImagePicker _picker = ImagePicker();
//   File? myFile;

//   @override
//   Widget build(BuildContext context) {
//     var size = MediaQuery.of(context).size;
//     return Scaffold(
//       appBar: AppBar(
//         elevation: 0,
//         actions: [
//           BlocListener<BlocPenerimaanKaderBloc, BlocPenerimaanKaderState>(
//             bloc:blocBio,
//             listener: (context, state) {
//               // TODO: implement listener
//               if(state is BlocStateSuksesPostPenerimaanKader){
//                 Navigator.pop(context);
//                 widget.callback!();
//               }
//             },
//             child: Container(
//               child: BlocBuilder<BlocPenerimaanKaderBloc, BlocPenerimaanKaderState>(
//                 bloc: blocBio,
//                 builder: (context, state) {
//                   if(state is BlocStateSuksesPostPenerimaanKader){
//                     return sendButton();
//                   }
//                   if(state is BlocStateLoadingPostPenerimaanKader){
//                     return Container(
//                       child: Center(
//                         child: CircularProgressIndicator(),
//                       ),
//                     );
//                   }
//                   if(state is BlocaStateFailedPostPenerimaanKader){
//                     return sendButton();
//                   }
//                   return sendButton();
//                 },
//               ),
//             ),
//           ),
//         ],
//         backgroundColor: blackBackground,
//         // title: Text("Biodata Kejuaraan"),
//       ),
//       body: Container(
//         color: blackBackground,
//         height: size.height,
//         width: size.width,
//         child: Column(
//           children: [
//             GestureDetector(
//               onTap: (){
//                 openGalerry();
//               },
//               child: Container(
//                       margin: EdgeInsets.all(5),
//                       height: size.height/6,
//                       width: size.width,
//                       decoration: BoxDecoration(
//                         color: blackMetalic,
//                         borderRadius: BorderRadius.circular(20)
//                       ),
//                       child: (myFile == null)?Icon(Icons.upload_file, color: captionColor2,size: size.width/6):Icon(Icons.file_present_sharp, color: captionColor2,size: size.width/6)
//                     ),
//             )
//           ],
//         ),
//       ),
//     );
//   }

//   IconButton sendButton() {
//     return IconButton(
//         icon: Icon(Icons.send),
//         onPressed: (){
//           ModelFormPenerimaanKader dataForm = ModelFormPenerimaanKader(file: myFile);
//           blocBio..add(PostPenerimaanKader(myForm: dataForm));
//         },
//       );
//   }

//   openGalerry()async{
//     // final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
//     // print(image!.name);
//     // print(image.readAsBytes());
//     FilePickerResult? result = await FilePicker.platform.pickFiles(withData: true,withReadStream: true,type: FileType.custom,
//   allowedExtensions: ['pdf'],);

//     if (result != null) {
//       print(result.paths);
//       PlatformFile file = result.files.first;

//       print(file.name);
//       print(file.bytes);
//       print(file.size);
//       print(file.extension);
//       print(file.path);
//       setState(() {
//         myFile = File(file.path!);
//       });
//     } else {
//       // User canceled the picker
//     }
//   }
// }