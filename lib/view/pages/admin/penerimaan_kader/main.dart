import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sistemts/model/model_penerimaan_kader/model_form.dart';
import 'package:sistemts/model/model_penerimaan_kader/model_list.dart';
import 'package:sistemts/view/pages/users/penerimaan_kader/tambah.dart';
import 'package:sistemts/view/util/color.dart';
import 'package:sistemts/view/widgets/dialog_delete/dialog_delete.dart';
import 'package:sistemts/view/widgets/home/buttoncustom1.dart';
import 'package:sistemts/view_model/api/method_download/main.dart';
import 'package:sistemts/view_model/bloc/bloc_penerimaan_kader/bloc_penerimaan_kader_bloc.dart';

class PenerimaanKaderAdmin extends StatefulWidget {
  const PenerimaanKaderAdmin({ Key? key }) : super(key: key);

  @override
  _PenerimaanKaderAdminState createState() => _PenerimaanKaderAdminState();
}

class _PenerimaanKaderAdminState extends State<PenerimaanKaderAdmin> {
  BlocPenerimaanKaderBloc blocBio = BlocPenerimaanKaderBloc();

  callbackTambah(){
    print("tambah");
    blocBio..add(GetAllPenerimaanKader());
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    blocBio..add(GetAllPenerimaanKader());
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: redColorBack,
      appBar: AppBar(
        elevation: 0,
        actions: [
        //   IconButton(
        //   icon: Icon(Icons.add),
        //   onPressed: (){
            
        //     Navigator.push(context, MaterialPageRoute(builder: (context)=>PageTambah(callback: callbackTambah,)));
        //   },
        // ),
        ],
        backgroundColor: blackBackground,
        // title: Text("Biodata Kejuaraan"),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: size.height,
          width: size.width,
          color: blackBackground,
          child: Column(
            children: [
              BlocListener<BlocPenerimaanKaderBloc, BlocPenerimaanKaderState>(
                bloc: blocBio,
                listener: (context, state) {
                  // TODO: implement listener
                  if(state is BlocStateSuksesVerifPenerimaanKader){
                    blocBio..add(GetAllPenerimaanKader());
                  }
                },
                child: Container(
                  child: BlocBuilder<BlocPenerimaanKaderBloc, BlocPenerimaanKaderState>(
                  bloc: blocBio,
                  builder: (context, state) {
                    // TODO: implement listener
                    if(state is BlocStateSuksesGetAllPenerimaanKader){
                      print(state.myData);
                      return listData(size,state.myData);
                    }
                    if(state is BlocStateLoadingGetAllPenerimaanKader){
                      return Container(
                      child:Center(
                        child:CircularProgressIndicator()
                      )
                    );
                    }
                    if(state is BlocaStateFailedGetAllPenerimaanKader){
                      return Container(
                      child:Center(
                        child:Text("Gagal Menampilkan Data")
                      )
                    );
                    }
                    if(state is BlocStateLoadingVerifPenerimaanKader){
                      return Container(
                      child:Center(
                        child:CircularProgressIndicator()
                      )
                    );
                    }
                    return Container(
                      child:Center(
                        child:CircularProgressIndicator()
                      )
                    );
                  },
                ),
                ),
              )
            ],
          ),
        )
      )
    );
  }

  ListView listData(Size size,List<ModelListPenerimaanKader>? myData) {
     
    return ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: myData!.length,
              itemBuilder: (BuildContext context,int i){
                return Container(
                  margin: EdgeInsets.all(5),
                  height: size.height/7,
                  width: size.width,
                  decoration: BoxDecoration(
                    color: blackMetalic,
                    borderRadius: BorderRadius.circular(20)

                  ),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: Icon(Icons.file_present, color: captionColor2,size: size.height/10,),
                      ),
                      Expanded(
                        flex: 3,
                        child: Container(
                          // color: Colors.red,
                          margin: EdgeInsets.all(20),
                          child: Column(
                            children: [
                              Expanded(
                                flex: 2,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(left:size.width/24),
                                      child: Text(myData[i].tglDaftar!.substring(0,10),style: TextStyle(color: captionColor2, fontSize: 16, fontWeight: FontWeight.bold,),)),
                                  ],
                                )),
                                Expanded(
                                flex: 2,
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: (myData[i].status ==0)?Icon(Icons.pending, color: Colors.blue,):Icon(Icons.check_circle,color: Colors.green,),
                                      ),
                                    Expanded(
                                      flex: 2,
                                      child: (myData[i].status ==0)?Text("Waiting", style: TextStyle(color: captionColor2, fontSize: 16, fontWeight: FontWeight.bold),):Text("Terverifikasi", style: TextStyle(color: captionColor2, fontSize: 16, fontWeight: FontWeight.bold),),
                                      ),
                                    
                                  ],
                                ),
                                )
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Column(
                          children: [
                            Expanded(
                              flex: 1,
                              child: button1(name: "Unduh",collorName: captionColor2,iconButton: Icons.download,collorIcon: Colors.green,collorButton: Colors.transparent,borderRadius: 88,fillCollor: redColorBack,clickCallback: ()=>_clickCallBack(context,myData[i].file!)),
                            ),
                            Expanded(
                              flex: 1,
                              child: button1(name: "Verif",collorName: captionColor2,iconButton: Icons.check_circle,collorIcon: Colors.green,collorButton: Colors.transparent,borderRadius: 88,fillCollor: redColorBack, clickCallback: ()=>_clickCallBackVerif(myData[i].idKader!),),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                );
              });
  }

  Future<void> _clickCallBack(BuildContext context, String filename) {
    print("download"); 
    return showCupertinoDialog(
              context: context,
              builder: (BuildContext context) => DownloadFile(filename: filename,)
            );
  }

  Future<void> _clickCallBackVerif(int id) {
    return showCupertinoDialog(
              context: context,
              builder: (BuildContext context) => DialogDelete(id:id,callback: callbackVerif,)
            );
  }

  callbackVerif(int id){
    print("delete");
    print(id);
    blocBio..add(VerifPenerimaanKader(id:id));
  }
}