import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sistemts/model/model_agenda/model_list_agenda.dart';
import 'package:sistemts/view/pages/admin/agenda/list_agenda/tambah.dart';
import 'package:sistemts/view/util/color.dart';
import 'package:sistemts/view/widgets/dialog_delete/dialog_delete.dart';
import 'package:sistemts/view_model/api/domain.dart';
import 'package:sistemts/view_model/bloc/bloc_agenda/bloc_agenda_bloc.dart';

class ListAgendaAdmin extends StatefulWidget {
  final int? id;
  ListAgendaAdmin({this.id});

  @override
  _ListAgendaAdminState createState() => _ListAgendaAdminState();
}

class _ListAgendaAdminState extends State<ListAgendaAdmin> {
  PageController pageController = PageController(keepPage: true);
  int currentPage=0;
  BlocAgendaBloc bloc = BlocAgendaBloc();
  BlocAgendaBloc blocHapus = BlocAgendaBloc();
  String url = urlDomain+"/controller/foto/";
  List<Map<String,String>> stepData =[
    {
      "text":"Selamat Datang di PPID LAN RI",
      "image":"assets/contoh/konten3.png"
    },
    {
      "text":"Kami akan membantu kalian untuk memberikan informasi layanan yang ada \ndi LAN RI",
      "image":"assets/contoh/konten1.png"
    },
    {
      "text":"Kami akan menyajikan informasi yang terbaru",
      "image":"assets/contoh/konten3.png"
    }
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc..add(EventGetAgendaByIdBidang(id: widget.id));
  }

  callbackAfterTambah(){
    print("tambah");
    bloc..add(EventGetAgendaByIdBidang(id: widget.id));
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>PageTambahAgenda(id: widget.id,callback: callbackAfterTambah,)));
        },
      ),
      backgroundColor: blackBackground,
      body: Container(
        child: BlocBuilder<BlocAgendaBloc, BlocAgendaState>(
          bloc: bloc,
          builder: (context, state) {
            if(state is BlocGetAgendaSuccessState){
              return ParentBody(state.listData!, size);
            }
            if(state is BlocGetAgendaLoadingState){
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
            if(state is BlocGetAgendaFailedState){
              return Container(
                child: Center(
                  child: Text("Gagal Memuat data"),
                ),
              );
            }
            return Container();
          },
        )
        
      ),
    );
  }

  Column ParentBody(List<ModelListAgenda> data, Size size) {
    return Column(
        children: [
          Expanded(
            flex: 5,
            child: ClipPath(
              clipper: MyClipper(),
              child: Container(
                color: Colors.red,
                child: Column(
                  children: [
                    Expanded(
                      flex: 8,
                      child: Stack(
                        children: [
                          PageView.builder(
                            physics: AlwaysScrollableScrollPhysics(),
                            controller: pageController,
                            onPageChanged: (val){
                                      setState(() {
                                        currentPage=val;
                                        // if(currentPage <= 1){
                                        //   val=currentPage;
                                        // }
                                      });      
                                      print(currentPage); 
                                    },
                            itemCount: data.length,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (BuildContext context, int i){
                              return ContenGambar(data);
                            }
                            ),
                            Positioned(
                              left: 10,
                              top: size.height/10,
                            child: Container(
                              alignment: Alignment.center,
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                color: redColorBack,
                                boxShadow: [
                                  BoxShadow(
                                    color: blackMetalic,
                                    spreadRadius: 3,
                                    blurRadius: 35,
                                    offset: Offset(3, 3), // changes position of shadow
                                  )
                                ],
                                borderRadius: BorderRadius.circular(50)
                              ),
                              
                              child: IconButton(
                                onPressed: (){
                                  Navigator.pop(context);
                                }, 
                                icon: Icon(Icons.arrow_back_ios_new_rounded,color: Colors.white, )),
                            )
                            ),
                            Positioned(
                              right: 10,
                              top: size.height/10,
                            child: Container(
                              alignment: Alignment.center,
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    color: blackMetalic,
                                    spreadRadius: 3,
                                    blurRadius: 35,
                                    offset: Offset(3, 3), // changes position of shadow
                                  )
                                ],
                                color: redColorBack,
                                borderRadius: BorderRadius.circular(50)
                              ),
                              
                              child: BlocListener<BlocAgendaBloc, BlocAgendaState>(
                                bloc: blocHapus,
                                listener: (context, state) {
                                  // TODO: implement listener
                                  if(state is BlocDeleteAgendaSuccessState){
                                    bloc..add(EventGetAgendaByIdBidang(id: widget.id));
                                  }
                                },
                                child: Container(
                                  child: BlocBuilder<BlocAgendaBloc, BlocAgendaState>(
                                    bloc: blocHapus,
                                    builder: (context, state) {
                                      if(state is BlocDeleteAgendaLoadingState){
                                        return Container(
                                          child: Center(
                                            child: CircularProgressIndicator(),
                                          ),
                                        );
                                      }
                                      if(state is BlocDeleteAgendaFailedState){
                                        return IconButton(
                                        onPressed: (){
                                          _clickCallBackDelete(data[currentPage].idAgenda!);
                                        }, 
                                        icon: Icon(Icons.delete,color: Colors.white, ));
                                      }
                                      if(state is BlocDeleteAgendaSuccessState){
                                        return IconButton(
                                        onPressed: (){
                                          _clickCallBackDelete(data[currentPage].idAgenda!);
                                        }, 
                                        icon: Icon(Icons.delete,color: Colors.white, ));
                                      }
                                      return IconButton(
                                        onPressed: (){
                                          _clickCallBackDelete(data[currentPage].idAgenda!);
                                        }, 
                                        icon: Icon(Icons.delete,color: Colors.white, ));
                                    },
                                  ),
                                ),
                              )
                              
                            )
                            ),
                        ],
                      )
                      
                    ),
                      Expanded(
                        flex: 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: List.generate(data.length,(index) => pagination(index)),)
                        )
                  ],
                ),
                ),
            )
              ),
            Expanded(
            flex: 1,
            child: Container(
              margin: EdgeInsets.all(20),
              color: Colors.transparent,
              child: PageView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        controller: pageController,
                        onPageChanged: (val){
                                  setState(() {
                                    currentPage=val;
                                    // if(currentPage <= 1){
                                    //   val=currentPage;
                                    // }
                                  });     
                                  print(currentPage); 
                                },
                        itemCount: data.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (BuildContext context, int i){
                          return ContentText(data);
                        }
                        ),
              )),
        ],
      );
  }

  Future<void> _clickCallBackDelete(int id) {
    return showCupertinoDialog(
              context: context,
              builder: (BuildContext context) => DialogDelete(id:id,callback: callbackDelete,)
            );
  }

  callbackDelete(int id){
    print("delete");
    print(id);
    blocHapus..add(EventDeleteAgenda(id: id));
  }

  Column ContentText(List<ModelListAgenda>data) => Column(
    children: [
      Text(data[currentPage].namaAgenda!, style: TextStyle(color: Colors.white,fontSize: 20,),),
    ],
  );

  Image ContenGambar(List<ModelListAgenda>data) => Image.network(url+data[currentPage].fotoKegiatan!,fit: BoxFit.cover,);

  Container pagination(index) {
    return Container(
                  margin: EdgeInsets.only(right:5),
                  height: 6,
                  width: (currentPage==index)?20:6,
                  decoration: BoxDecoration(
                    color: (currentPage==index)?blackBackground:captionColor,
                    borderRadius: BorderRadius.circular(3)
                  ),
                );
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size){
    var path = new Path();
    path.lineTo(0, size.height/2);
    var controllPoint = Offset(size.width/8, size.height);
    var endPoint= Offset(size.width/2, size.height);
    path.quadraticBezierTo(controllPoint.dx, controllPoint.dy, endPoint.dx,endPoint.dy);
    path.quadraticBezierTo(350, controllPoint.dy, size.width,endPoint.dy/2);
    // path.lineTo(size.width, size.height);
    
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path>oldClipper){
    return true;
  }
  
}