import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sistemts/model/model_agenda/model_form_agenda.dart';
import 'package:sistemts/model/model_penerimaan_kader/model_form.dart';
import 'package:sistemts/view/util/color.dart';
import 'package:image_picker/image_picker.dart';
import 'package:file_picker/file_picker.dart';
import 'package:sistemts/view/util/register/register/custom_text.dart';
import 'package:sistemts/view_model/bloc/bloc_agenda/bloc_agenda_bloc.dart';
import 'package:sistemts/view_model/bloc/bloc_penerimaan_kader/bloc_penerimaan_kader_bloc.dart';

typedef TambahData = void Function();
class PageTambahAgenda extends StatefulWidget {
  final int? id;
  final TambahData? callback;
  PageTambahAgenda({this.callback, this.id});

  @override
  _PageTambahAgendaState createState() => _PageTambahAgendaState();
}

class _PageTambahAgendaState extends State<PageTambahAgenda> {
  BlocAgendaBloc blocBio = BlocAgendaBloc();
  final ImagePicker _picker = ImagePicker();
  TextEditingController _namaAgenda = TextEditingController();
  File? myFile;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        actions: [
          BlocListener<BlocAgendaBloc, BlocAgendaState>(
            bloc:blocBio,
            listener: (context, state) {
              // TODO: implement listener
              if(state is BlocPostAgendaSuccessState){
                Navigator.pop(context);
                widget.callback!();
              }
            },
            child: Container(
              child: BlocBuilder<BlocAgendaBloc, BlocAgendaState>(
                bloc: blocBio,
                builder: (context, state) {
                  if(state is BlocPostAgendaSuccessState){
                    return sendButton();
                  }
                  if(state is BlocPostAgendaLoadingState){
                    return Container(
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    );
                  }
                  if(state is BlocPostAgendaFailedState){
                    return sendButton();
                  }
                  return sendButton();
                },
              ),
            ),
          ),
        ],
        backgroundColor: blackBackground,
        // title: Text("Biodata Kejuaraan"),
      ),
      body: Container(
        color: blackBackground,
        height: size.height,
        width: size.width,
        child: Column(
          children: [
            GestureDetector(
              onTap: (){
                openGalerry();
              },
              child: Container(
                      margin: EdgeInsets.all(5),
                      height: size.height/6,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: blackMetalic,
                        borderRadius: BorderRadius.circular(20)
                      ),
                      child: (myFile == null)?Icon(Icons.upload_file, color: captionColor2,size: size.width/6):Icon(Icons.file_present_sharp, color: captionColor2,size: size.width/6)
                    ),
            ),
            SizedBox(height: 10,),
            TextFieldRegister(
                                typePass: false,
                                typeInput: false,
                                isiField: _namaAgenda,
                                hintText: "Agenda",
                                prefixIcon: Icons.view_agenda_sharp,
                              ),
          ],
        ),
      ),
    );
  }

  IconButton sendButton() {
    return IconButton(
        icon: Icon(Icons.send),
        onPressed: (){
          ModelFormAgenda dataForm = ModelFormAgenda(namaAgenda: _namaAgenda.text, bidang: widget.id, fotoKegiatan: myFile, fotoPengurus: myFile);
          blocBio..add(EventPostAgenda(data: dataForm));
        },
      );
  }

  openGalerry()async{
    // final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    // print(image!.name);
    // print(image.readAsBytes());
    FilePickerResult? result = await FilePicker.platform.pickFiles(withData: true,withReadStream: true,type: FileType.custom,
  allowedExtensions: ['jpg','png'],);

    if (result != null) {
      print(result.paths);
      PlatformFile file = result.files.first;

      print(file.name);
      print(file.bytes);
      print(file.size);
      print(file.extension);
      print(file.path);
      setState(() {
        myFile = File(file.path!);
      });
    } else {
      // User canceled the picker
    }
  }
}