import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sistemts/model/model_users/model_list_user.dart';
import 'package:sistemts/view/pages/admin/peserta/list_view/main.dart';
import 'package:sistemts/view/util/color.dart';
import 'package:sistemts/view/widgets/dialog_verif/dialog_verif.dart';
import 'package:sistemts/view/widgets/home/buttoncustom1.dart';
import 'package:sistemts/view_model/bloc/bloc_users/bloc_users_bloc.dart';

class PagePesertaAdmin extends StatefulWidget {
  const PagePesertaAdmin({ Key? key }) : super(key: key);

  @override
  _PagePesertaAdminState createState() => _PagePesertaAdminState();
}

class _PagePesertaAdminState extends State<PagePesertaAdmin> {
  BlocUsersBloc bloc = BlocUsersBloc();
  BlocUsersBloc blocVerif = BlocUsersBloc();
  int? jumlahJadwal;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc..add(EventGetAllUser());
  }

  @override
  Widget build(BuildContext context) {
    var size= MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: blackBackground,
      body: Container(
        color: Colors.transparent,
        margin: EdgeInsets.all(20),
      child: Column(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.only(top:size.height/16),
              color: Colors.transparent,
              child: Row(
                children: [
                  Expanded(
                    flex: 3,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Text("Daftar", style: TextStyle(color: captionColor2, fontSize: 30, fontWeight: FontWeight.normal),)),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Text("Peserta", style: TextStyle(color: captionColor2, fontSize: 30, fontWeight: FontWeight.bold),))
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.topCenter,
                          height: size.height/10,
                          decoration: BoxDecoration(
                          border: Border.all(color: redColorBack, width: 3),
                            color: Colors.transparent, 
                            borderRadius: BorderRadius.circular(20)),
                            child: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(jumlahJadwal.toString(), style: TextStyle(color: captionColor2, fontSize: 20, fontWeight: FontWeight.normal)),
                                  Text("Peserta", style: TextStyle(color: captionColor2, fontSize: 20, fontWeight: FontWeight.normal)),
                                ],
                              ),
                            ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 4,
            child: Container(
              width: size.width,
              decoration: BoxDecoration(
                            color: Colors.transparent, 
                            borderRadius: BorderRadius.circular(20)),
              child: BlocListener<BlocUsersBloc, BlocUsersState>(
                bloc: bloc,
                listener: (context, state) {
                  // TODO: implement listener
                  if(state is BlocGetAllUsersSuccessState){
                    setState(() {
                      jumlahJadwal=state.listData!.length;
                    });
                  }
                },
                child: Container(
                  child: BlocBuilder<BlocUsersBloc, BlocUsersState>(
                bloc: bloc,
                builder: (context, state) {
                  if(state is BlocGetAllUsersSuccessState){
                    return ListViewChild(size, state.listData);
                  }
                  if(state is BlocGetAllUsersLoadingState){
                    return Container(child: Center(child: CircularProgressIndicator(),),);
                  }
                  if(state is BlocGetAllUsersFailedState){
                    return Container(child: Center(child: Text("Tidak dapat menampilkan data"),),);
                  }
                  return Container();
                },
              ),
                ),
              )
            ),
          )
        ],
      ),
    ),
    );
  }

  ListView ListViewChild(Size size, List<ModelListUser>? data) {
    return ListView.builder(
              shrinkWrap: true,
              physics: AlwaysScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              itemCount: data!.length,
              itemBuilder: (BuildContext context, int i){
                return Container(
                  margin: EdgeInsets.all(5),
                  height: size.height/6.8,
                  width: size.width,
                  decoration: BoxDecoration(
                        border: Border.all(color: blackMetalic, width: 2),
                          color: blackMetalic, 
                          borderRadius: BorderRadius.circular(20)),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 4,
                                child: Container(
                                  margin: EdgeInsets.all(15),
                                  // color: Colors.transparent,
                                  child: Column(
                                    children: [
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text("Nama : "+data[i].namaUser.toString(), style: TextStyle(color: captionColor2, fontSize: 20, fontWeight: FontWeight.bold))),
                                        )
                                    ],
                                  ),
                                )),
                                Expanded(
                                flex: 2,
                                child: Column(
                                  children: [
                                    Container(
                                      // flex: 1,
                                      child: button1(name: "View",collorName: captionColor2,iconButton: Icons.view_list,collorIcon: Colors.blue,collorButton: captionColor,borderRadius: 88,fillCollor: Colors.transparent,clickCallback: ()=>_clickCallBackView(data[i].idUser)),
                                    ),
                                    Container(
                                      // flex: 1,
                                      child: BlocListener<BlocUsersBloc, BlocUsersState>(
                                        bloc: blocVerif,
                                        listener: (context, state) {
                                          // TODO: implement listener
                                          if(state is BlocVerifUsersSuccessState){
                                            print("sukses");
                                            bloc..add(EventGetAllUser());
                                          }
                                        },
                                        child: Container(
                                          child: BlocBuilder<BlocUsersBloc, BlocUsersState>(
                                        bloc: blocVerif,
                                        builder: (context, state) {
                                          if(state is BlocVerifUsersFailedState){
                                            return button1(name: "Verif",collorName: captionColor2,iconButton: Icons.check_sharp,collorIcon: Colors.green,collorButton: captionColor,borderRadius: 88,fillCollor: Colors.transparent, clickCallback: ()=>_clickCallBackVerif(data[i].idUser!),);
                                          }
                                          if(state is BlocVerifUsersLoadingState){
                                            return Container(
                                              child: Center(
                                                child: CircularProgressIndicator(),
                                              ),
                                            );
                                          }
                                          if(state is BlocVerifUsersSuccessState){
                                            return button1(name: "Verif",collorName: captionColor2,iconButton: Icons.check_sharp,collorIcon: Colors.green,collorButton: captionColor,borderRadius: 88,fillCollor: Colors.transparent, clickCallback: ()=>_clickCallBackVerif(data[i].idUser!),);
                                          }
                                          return button1(name: "Verif",collorName: captionColor2,iconButton: Icons.check_sharp,collorIcon: Colors.green,collorButton: captionColor,borderRadius: 88,fillCollor: Colors.transparent, clickCallback: ()=>_clickCallBackVerif(data[i].idUser!),);
                                        },
                                      ),
                                        ),
                                      )
                                    )
                                  ],
                                ),
                              ),
                                Expanded(
                                flex: 1,
                                child: Container(
                                  decoration: BoxDecoration(
                                  // border: Border.all(color: blackMetalic, width: 2),
                                    color: (data[i].status == 1)?Colors.green:Colors.red, 
                                    borderRadius: BorderRadius.only(topRight: Radius.circular(20),bottomRight: Radius.circular(20))),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      (data[i].status == 1)?Icon(Icons.check_circle_outline_sharp, color: Colors.white, size: 30,):Icon(Icons.cancel_rounded, color: Colors.white, size: 30,)
                                    ],
                                  )
                                ))
                            ],
                          ),
                );
              }
              );
  }

  Future<void> _clickCallBackView(id) {
    print("download"); 
    return Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>ViewPeserta(id:id)));
  }

  Future<void> _clickCallBackVerif(int id) {
    return showCupertinoDialog(
              context: context,
              builder: (BuildContext context) => DialogVerif(id:id,callback: callbackVerif,)
            );
  }

  callbackVerif(int id){
    print("delete");
    print(id);
    blocVerif..add(EventVerifUser(id: id));
  }
}