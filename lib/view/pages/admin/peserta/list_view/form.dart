import 'package:sistemts/model/model_register/form_register_user.dart';
import 'package:sistemts/view/pages/users/home_users.dart';
import 'package:sistemts/view/util/color.dart';
import 'package:sistemts/view/util/login/login/custom_button.dart';
import 'package:sistemts/view/util/login/login/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sistemts/view/util/register/register/custom_button.dart';
import 'package:sistemts/view/util/register/register/custom_text.dart';
import 'package:sistemts/view_model/bloc/bloc_register/register_bloc.dart';
import 'package:sistemts/view_model/bloc/bloc_users/bloc_users_bloc.dart';

class FormMainAdmin extends StatefulWidget {
  final int? id;
  FormMainAdmin({ this.id});

  @override
  _FormMainAdminState createState() => _FormMainAdminState();
}

class _FormMainAdminState extends State<FormMainAdmin> {
  final TextEditingController _nim = TextEditingController();
  final TextEditingController _nama_user = TextEditingController();
  final TextEditingController _no_hp = TextEditingController();
  final TextEditingController _tempat_lahir= TextEditingController();
  final TextEditingController _tgl_lahir= TextEditingController();
  final TextEditingController _alamat = TextEditingController();
  final TextEditingController _jenis_kelamin = TextEditingController();
  final TextEditingController _agama =TextEditingController();
  final TextEditingController _tinggi_badan =TextEditingController();
  final TextEditingController _fakultas_jurusan =TextEditingController();
  final TextEditingController _nama_ayah = TextEditingController();
  final TextEditingController _nama_ibu = TextEditingController();
  final TextEditingController _berat_badan = TextEditingController();
  int? status;

  BlocUsersBloc blocUser = BlocUsersBloc();
  RegisterBloc bloc = RegisterBloc();
  bool statusPass = true;
  void _clickCallBack() {
    setState(() {
      statusPass = !statusPass;
    });
  }

  @override
  void initState() {
    super.initState();
    // bloc..add(BlocEventDisconnect());
    blocUser..add(EventGetUsersById(id: widget.id));
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return BlocListener<BlocUsersBloc, BlocUsersState>(
      bloc: blocUser,
      listener: (context, state) {
        // TODO: implement listener
        if(state is BlocGetUsersSuccessState){
          setState(() {
            _nim.text = state.listData![0].nim!;
            _nama_user.text = state.listData![0].namaUser!;
            _no_hp.text = state.listData![0].noHp!;
            _tempat_lahir.text = state.listData![0].tempatTglLahir!;
            _alamat.text = state.listData![0].alamat!;
            _jenis_kelamin.text = state.listData![0].jenisKelamin!;
            _agama.text =state.listData![0].agama!;
            _tinggi_badan.text =state.listData![0].tinggiBadan!;
            _fakultas_jurusan.text =state.listData![0].fakultasJurusan!;
            _nama_ayah.text = state.listData![0].namaAyah!;
            _nama_ibu.text = state.listData![0].namaIbu!;
            _berat_badan.text = state.listData![0].beratBadan!;
            status = state.listData![0].status!;
          });
        }
      },
      child: Container(
        child: BlocBuilder<BlocUsersBloc, BlocUsersState>(
          bloc: blocUser,
          builder: (context, state) {
            if(state is BlocGetUsersFailedState){
              return FormProfil(size);
            }
            if(state is BlocGetUsersLoadingState){
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
            if(state is BlocGetUsersSuccessState){
              return FormProfil(size);
            }
            return FormProfil(size);
          },
        ),
      ),
    );
  }

  Container FormProfil(Size size) {
    return Container(
              padding:const EdgeInsets.symmetric(vertical: 70),
              alignment: Alignment.center,
              child: Column(
                  children: [
                    Container(
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          (status == 1)?Icon(Icons.check_circle_outline_rounded, color: Colors.green,size: 34,):Icon(Icons.cancel_outlined, color: redColorBack,size: 34,),
                          SizedBox(width:10),
                          (status == 1)?Text("Akun Terverifikasi", style: TextStyle(color: Colors.green, fontSize: 18),):Text("Akun Belum Terverifikasi", style: TextStyle(color: redColorBack, fontSize: 18),)
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextFieldRegister(
                      typePass: false,
                      typeInput: false,
                      isiField: _nim,
                      hintText: "NIM",
                    ),
                    TextFieldRegister(
                      typePass: false,
                      typeInput: false,
                      isiField: _nama_user,
                      hintText: "Nama Lengkap",
                    ),
                    TextFieldRegister(
                      typePass: false,
                      typeInput: true,
                      isiField: _no_hp,
                      hintText: "Nomor Hp",
                    ),
                    TextFieldRegister(
                      typePass: false,
                      typeInput: false,
                      isiField: _tempat_lahir,
                      hintText: "Tempat Lahir",
                    ),
                    TextFieldRegister(
                      typePass: false,
                      typeInput: false,
                      isiField: _alamat,
                      hintText: "Alamat",
                    ),
                    TextFieldRegister(
                      typePass: false,
                      typeInput: false,
                      isiField: _jenis_kelamin,
                      hintText: "Jenis Kelamin",
                    ),
                    TextFieldRegister(
                      typePass: false,
                      typeInput: false,
                      isiField: _agama,
                      hintText: "Agama",
                    ),
                    TextFieldRegister(
                      typePass: false,
                      typeInput: false,
                      isiField: _tinggi_badan,
                      hintText: "Tinggi Badan",
                    ),
                    TextFieldRegister(
                      typePass: false,
                      typeInput: false,
                      isiField: _berat_badan,
                      hintText: "Berat Badan",
                    ),
                    TextFieldRegister(
                      typePass: false,
                      typeInput: false,
                      isiField: _fakultas_jurusan,
                      hintText: "Fakultas Jurusan",
                    ),
                    TextFieldRegister(
                      typePass: false,
                      typeInput: false,
                      isiField: _nama_ayah,
                      hintText: "Nama Ayah",
                    ),
                    TextFieldRegister(
                      typePass: false,
                      typeInput: false,
                      isiField: _nama_ibu,
                      hintText: "Nama Ibu",
                    ),
                    SizedBox(height: 20,),
                    // Container(
                    //   width: size.width,
                    //   child: Row(
                    //     mainAxisAlignment: MainAxisAlignment.spaceAround,
                    //     children: [
                    //       BlocListener<BlocUsersBloc, BlocUsersState>(
                    //       bloc: blocUser,
                    //       listener: (context, state) {
                    //         if(state is BlocPutUsersSuccessState){
                    //           ScaffoldMessenger.of(context).showSnackBar(
                    //             SnackBar(
                    //               content: Text("Register Berhasil"),
                    //               duration: Duration(milliseconds: 500),
                    //               backgroundColor: Colors.green,
                    //               onVisible: (){
                    //                 blocUser..add(EventGetUsersById());
                    //               },
                    //             )
                    //           );
                    //         }
                    //         if(state is BlocPutUsersFailedState){
                    //           ScaffoldMessenger.of(context).showSnackBar(
                    //             SnackBar(
                    //               content: Text("Register Gagal, "+state.errorMessage.toString()),
                    //               duration: Duration(milliseconds: 2500),
                    //               backgroundColor: Colors.red,
                    //             )
                    //           );
                    //         }
                    //       },
                    //       child: Container(
                    //         child: BlocBuilder<BlocUsersBloc, BlocUsersState>(
                    //           bloc: blocUser,
                    //           builder: (context, state) {
                    //             if (state is BlocPutUsersSuccessState) {
                    //               return buildButtonRegister();
                    //             }
                    //             if (state is BlocPutUsersLoadingState) {
                    //               return Center(
                    //                 child: CircularProgressIndicator(color: Colors.white,),
                    //               );
                    //             }
                    //             if (state is BlocPutUsersFailedState) {
                    //               return buildButtonRegister();
                    //             }
                    //             return buildButtonRegister();
                    //           },
                    //         ),
                    //       ),
                    //     ),
                    //     ],
                    //   ),
                    // ),
                    // Spacer()
                  ],
                ),
            );
  }

  ButtonRegister buildButtonRegister() {
    return ButtonRegister(
      onPress: (){
        print("sign");
        registUser formData = registUser(nim: _nim.text, namaUser: _nama_user.text, noHp: _no_hp.text,tempatTglLahir: _tempat_lahir.text+"; "+_tgl_lahir.text, jenisKelamin: _jenis_kelamin.text,agama: _agama.text,tinggiBadan: _tinggi_badan.text, beratBadan: _berat_badan.text, fakultasJurusan: _fakultas_jurusan.text,namaAyah: _nama_ayah.text, namaIbu: _nama_ibu.text, alamat: _alamat.text);
        blocUser..add(EventPutUser(data: formData));
      },
      buttonName: "Simpan",
      paddingH: 35.0,
    );
  }
}