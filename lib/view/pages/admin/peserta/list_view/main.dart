import 'package:flutter/material.dart';
import 'package:sistemts/view/pages/admin/peserta/list_view/form.dart';
import 'package:sistemts/view/util/color.dart';

class ViewPeserta extends StatefulWidget {
  final int? id;
  ViewPeserta({ this.id});

  @override
  _ViewPesertaState createState() => _ViewPesertaState();
}

class _ViewPesertaState extends State<ViewPeserta> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      backgroundColor: blackBackground,
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        physics: AlwaysScrollableScrollPhysics(),
        child: Container(
          child: FormMainAdmin(id: widget.id,),
        ),
      ),
    );
  }
}