
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sistemts/model/model_jadwal/model_list_jadwal.dart';
import 'package:sistemts/view/util/color.dart';
import 'package:sistemts/view/util/register/register/custom_text.dart';
import 'package:sistemts/view_model/bloc/bloc_jadwal/bloc_jadwal_bloc.dart';

typedef TambahData = void Function();
class PageTambahJadwal extends StatefulWidget {
  final int? id;
  final TambahData? callback;
  PageTambahJadwal({this.callback, this.id});

  @override
  _PageTambahJadwalState createState() => _PageTambahJadwalState();
}

class _PageTambahJadwalState extends State<PageTambahJadwal> {
  BlocJadwalBloc blocBio = BlocJadwalBloc();
  TextEditingController _waktu = TextEditingController();
  TextEditingController _tempat = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        elevation: 0,
        actions: [
          BlocListener<BlocJadwalBloc, BlocJadwalState>(
            bloc:blocBio,
            listener: (context, state) {
              // TODO: implement listener
              if(state is BlocPostJadwalSuccessState){
                widget.callback!();
                Navigator.pop(context);
              }
            },
            child: Container(
              child: BlocBuilder<BlocJadwalBloc, BlocJadwalState>(
                bloc: blocBio,
                builder: (context, state) {
                  if(state is BlocPostJadwalSuccessState){
                    return sendButton();
                  }
                  if(state is BlocPostJadwalLoadingState){
                    return Container(
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    );
                  }
                  if(state is BlocPostJadwalFailedState){
                    return sendButton();
                  }
                  return sendButton();
                },
              ),
            ),
          ),
        ],
        backgroundColor: blackBackground,
        // title: Text("Biodata Kejuaraan"),
      ),
      body: Container(
        color: blackBackground,
        height: size.height,
        width: size.width,
        child: Column(
          children: [
            TextFieldRegister(
                                typePass: false,
                                typeInput: false,
                                isiField: _tempat,
                                hintText: "Tempat",
                                prefixIcon: Icons.view_agenda_sharp,
                              ),
                              TextFieldRegister(
                                typePass: false,
                                typeInput: false,
                                isiField: _waktu,
                                hintText: "Waktu",
                                prefixIcon: Icons.view_agenda_sharp,
                              ),
          ],
        ),
      ),
    );
  }

  IconButton sendButton() {
    return IconButton(
        icon: Icon(Icons.send),
        onPressed: (){
          ModelListJadwal dataForm = ModelListJadwal(tempat: _tempat.text, waktu: _waktu.text);
          blocBio..add(EventPostJadwal(data: dataForm));
        },
      );
  }

}