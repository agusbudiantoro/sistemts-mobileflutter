import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef VerifData = void Function(int);
class DialogVerif extends StatefulWidget {
  final VerifData? callback;
  final int? id;
  DialogVerif({ this.callback, this.id});

  @override
  _DialogVerifState createState() => _DialogVerifState();
}

class _DialogVerifState extends State<DialogVerif> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: CupertinoAlertDialog(
                title: const Text('Verifikasi Akun'),
                content: const Text('Apakah anda yakin ingin memverifikasi data ini ?'),
                actions: <CupertinoDialogAction>[
                  CupertinoDialogAction(
                    child: const Text('No'),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  CupertinoDialogAction(
                    child: const Text('Yes'),
                    isDestructiveAction: true,
                    onPressed: () {
                      widget.callback!(widget.id!);
                      Navigator.pop(context);
                      // Do something destructive.
                    },
                  )
                ],
              ),
    );
  }
}