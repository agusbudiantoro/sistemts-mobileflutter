import 'package:sistemts/view/util/color.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

// ignore: use_key_in_widget_constructors
class TopBar extends StatefulWidget {
  @override
  _TopBarState createState() => _TopBarState();
}

class _TopBarState extends State<TopBar> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return AppBar(
      backgroundColor: blackMetalic,
      centerTitle: false,
      titleSpacing: 5,
      automaticallyImplyLeading: false,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // ignore: prefer_const_constructors
          GestureDetector(
            onTap: (){
              Navigator.pop(context);
            },
            child: Container(
              width: size.width/18,
              child: const Icon(Icons.arrow_back, color: captionColor,),
            ),
          ),
          Container(
          width: size.width / 6,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(50.0),
            child: Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                  border: Border.all(width: 3, color: captionColor)),
              child: Padding(
                padding: const EdgeInsets.all(3.0),
                child: CircleAvatar(
                  backgroundImage: const CachedNetworkImageProvider(
                      "https://upload.wikimedia.org/wikipedia/commons/a/a0/Arh-avatar.jpg",
                      scale: 10),
                  radius: size.width / 20,
                ),
              ),
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.fromLTRB(5, 10, 10, 5),
          alignment: Alignment.topLeft,
          height: size.height / 12,
          width: ((size.width - 18) - (size.width / 6)) / 1.3,
          child: Column(
            // ignore: prefer_const_literals_to_create_immutables
            children: [
              // ignore: avoid_unnecessary_containers
              Container(
                alignment: Alignment.topLeft,
                child: const Text("Ceps",
                    style: TextStyle(color: captionColor, fontSize: 16)),
              ),
              Container(
                alignment: Alignment.topLeft,
                child: const Text("Online",
                    style: TextStyle(color: captionColor, fontSize: 14)),
              ),
            ],
          ),
        ),
        GestureDetector(
            onTap: (){
              Navigator.pop(context);
            },
            child: Container(
              width: size.width/7,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                // ignore: prefer_const_literals_to_create_immutables
                children: [
                  const Icon(Icons.more_vert_outlined, color: captionColor,),
                ],
              ),
            ),
          ),
        ],
        ),
    );
  }
}