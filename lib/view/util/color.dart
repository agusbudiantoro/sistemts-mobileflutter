import 'package:flutter/material.dart';

const Color blackMetalic = Color(0xFF1b1b1b); 
const Color captionColor = Color(0xFFAEA2A7);
const Color captionColor2 = Colors.white;
const Color redColorBack = Color(0xFFDF0505);
const Color redColorBlack2 = Color(0xFFC25D53);

const Color blackBackground = Color(0xFF121212);
const Color button = Color(0xFFFFD2D2);