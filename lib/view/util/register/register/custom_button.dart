import 'package:flutter/material.dart';

class ButtonRegister extends StatefulWidget {
  final String? buttonName;
  final Function? onPress;
  final double? paddingH;

  ButtonRegister({this.buttonName, this.onPress, this.paddingH});
  @override
  _ButtonRegisterState createState() => _ButtonRegisterState();
}

class _ButtonRegisterState extends State<ButtonRegister> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        widget.onPress!();
      },
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: 8.0,
          horizontal: widget.paddingH!
        ),
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.white,
            width: 2
          ), 
          borderRadius: BorderRadius.all(Radius.circular(25.0))
        ),
        child: Text(
          widget.buttonName!,
          style: TextStyle(color: Colors.white, fontSize: 20.0),
        ),
      ),
    );
  }
}