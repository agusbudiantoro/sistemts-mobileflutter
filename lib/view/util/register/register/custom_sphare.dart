import 'package:flutter/material.dart';

class SphareRegister extends StatefulWidget {
  final double? width;
  final double? height;

  SphareRegister({this.height, this.width});
  @override
  _SphareRegisterState createState() => _SphareRegisterState();
}

class _SphareRegisterState extends State<SphareRegister> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height,
      width: widget.width,
      decoration: BoxDecoration(
        // gradient:LinearGradient(
        //   colors: [
        //     circlePurpleDark,
        //     circlePurpleLight
        //   ]
        // ),
        shape: BoxShape.circle
      ),
    );
  }
}