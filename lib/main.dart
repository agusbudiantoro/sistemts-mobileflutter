import 'package:sistemts/splash.dart';
import 'package:sistemts/view/pages/login/main.dart';
import 'package:sistemts/view/util/color.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';


void main() {
  runApp(MaterialApp(
    theme: ThemeData(
    colorScheme: ColorScheme.fromSwatch(
      primarySwatch: Colors.red,
    ).copyWith(
      secondary: Colors.red,
    )
    ),
    home: OPSplashScreen(),
  ));
}


