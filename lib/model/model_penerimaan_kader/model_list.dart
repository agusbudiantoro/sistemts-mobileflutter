class ModelListPenerimaanKader {
  int? idKader;
  int? idUser;
  String? tglDaftar;
  String? file;
  int? status;

  ModelListPenerimaanKader(
      {this.idKader, this.idUser, this.tglDaftar, this.file, this.status});

  ModelListPenerimaanKader.fromJson(Map<String, dynamic> json) {
    idKader = json['id_kader'];
    idUser = json['id_user'];
    tglDaftar = json['tgl_daftar'];
    file = json['file'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_kader'] = this.idKader;
    data['id_user'] = this.idUser;
    data['tgl_daftar'] = this.tglDaftar;
    data['file'] = this.file;
    data['status'] = this.status;
    return data;
  }
}