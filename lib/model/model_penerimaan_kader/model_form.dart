import 'dart:io';

class ModelFormPenerimaanKader{
  int? id_kader;
  int? id_user;
  File? file;
  int? status;
  ModelFormPenerimaanKader({this.file, this.id_kader, this.id_user, this.status});

  get path => null;
}