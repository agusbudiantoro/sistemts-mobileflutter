class ModelListBidang {
  int? idBidang;
  String? namaBidang;
  int? jumlahAgenda;

  ModelListBidang({this.idBidang, this.namaBidang, this.jumlahAgenda});

  ModelListBidang.fromJson(Map<String, dynamic> json) {
    idBidang = json['id_bidang'];
    namaBidang = json['nama_bidang'];
    jumlahAgenda = json['jumlah_agenda'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_bidang'] = this.idBidang;
    data['nama_bidang'] = this.namaBidang;
    data['jumlah_agenda'] = this.jumlahAgenda;
    return data;
  }
}