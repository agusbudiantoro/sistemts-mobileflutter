import 'dart:io';

class ModelFormBiodataKejuaraan{
  int? id_kejuaraan;
  int? id_user;
  File? file;
  int? status;
  ModelFormBiodataKejuaraan({this.file, this.id_kejuaraan, this.id_user, this.status});

  get path => null;
}