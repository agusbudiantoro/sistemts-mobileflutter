class ModelListBiodataKejuaraan {
  int? idKejuaraan;
  int? idUser;
  String? tglDaftar;
  String? file;
  int? status;

  ModelListBiodataKejuaraan(
      {this.idKejuaraan, this.idUser, this.tglDaftar, this.file, this.status});

  ModelListBiodataKejuaraan.fromJson(Map<String, dynamic> json) {
    idKejuaraan = json['id_kejuaraan'];
    idUser = json['id_user'];
    tglDaftar = json['tgl_daftar'];
    file = json['file'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_kejuaraan'] = this.idKejuaraan;
    data['id_user'] = this.idUser;
    data['tgl_daftar'] = this.tglDaftar;
    data['file'] = this.file;
    data['status'] = this.status;
    return data;
  }
}