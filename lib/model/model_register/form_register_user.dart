class registUser {
  String? nim;
  String? namaUser;
  String? email;
  String? noHp;
  String? tempatTglLahir;
  String? alamat;
  String? jenisKelamin;
  String? agama;
  String? tinggiBadan;
  String? fakultasJurusan;
  String? namaAyah;
  String? namaIbu;
  int? status;
  String? beratBadan;
  String? password;

  registUser(
      {this.nim,
      this.namaUser,
      this.email,
      this.noHp,
      this.tempatTglLahir,
      this.alamat,
      this.jenisKelamin,
      this.agama,
      this.tinggiBadan,
      this.fakultasJurusan,
      this.namaAyah,
      this.namaIbu,
      this.status,
      this.beratBadan,
      this.password});

  registUser.fromJson(Map<String, dynamic> json) {
    nim = json['nim'];
    namaUser = json['nama_user'];
    email = json['email'];
    noHp = json['no_hp'];
    tempatTglLahir = json['tempat_tgl_lahir'];
    alamat = json['alamat'];
    jenisKelamin = json['jenis_kelamin'];
    agama = json['agama'];
    tinggiBadan = json['tinggi_badan'];
    fakultasJurusan = json['fakultas_jurusan'];
    namaAyah = json['nama_ayah'];
    namaIbu = json['nama_ibu'];
    status = json['status'];
    beratBadan = json['berat_badan'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nim'] = nim;
    data['nama_user'] = namaUser;
    data['email'] = email;
    data['no_hp'] = noHp;
    data['tempat_tgl_lahir'] = tempatTglLahir;
    data['alamat'] = alamat;
    data['jenis_kelamin'] = jenisKelamin;
    data['agama'] = agama;
    data['tinggi_badan'] = tinggiBadan;
    data['fakultas_jurusan'] = fakultasJurusan;
    data['nama_ayah'] = namaAyah;
    data['nama_ibu'] = namaIbu;
    data['status'] = status;
    data['berat_badan'] = beratBadan;
    data['password'] = password;
    return data;
  }
}