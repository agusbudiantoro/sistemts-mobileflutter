class ModelListUser {
  int? idUser;
  String? namaUser;
  String? email;
  String? noHp;
  String? tglDaftar;
  String? tempatTglLahir;
  String? alamat;
  String? jenisKelamin;
  String? agama;
  String? tinggiBadan;
  String? fakultasJurusan;
  String? nim;
  String? namaAyah;
  String? namaIbu;
  int? status;
  String? beratBadan;

  ModelListUser(
      {this.idUser,
      this.namaUser,
      this.email,
      this.noHp,
      this.tglDaftar,
      this.tempatTglLahir,
      this.alamat,
      this.jenisKelamin,
      this.agama,
      this.tinggiBadan,
      this.fakultasJurusan,
      this.nim,
      this.namaAyah,
      this.namaIbu,
      this.status,
      this.beratBadan});

  ModelListUser.fromJson(Map<String, dynamic> json) {
    idUser = json['id_user'];
    namaUser = json['nama_user'];
    email = json['email'];
    noHp = json['no_hp'];
    tglDaftar = json['tgl_daftar'];
    tempatTglLahir = json['tempat_tgl_lahir'];
    alamat = json['alamat'];
    jenisKelamin = json['jenis_kelamin'];
    agama = json['agama'];
    tinggiBadan = json['tinggi_badan'];
    fakultasJurusan = json['fakultas_jurusan'];
    nim = json['nim'];
    namaAyah = json['nama_ayah'];
    namaIbu = json['nama_ibu'];
    status = json['status'];
    beratBadan = json['berat_badan'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_user'] = this.idUser;
    data['nama_user'] = this.namaUser;
    data['email'] = this.email;
    data['no_hp'] = this.noHp;
    data['tgl_daftar'] = this.tglDaftar;
    data['tempat_tgl_lahir'] = this.tempatTglLahir;
    data['alamat'] = this.alamat;
    data['jenis_kelamin'] = this.jenisKelamin;
    data['agama'] = this.agama;
    data['tinggi_badan'] = this.tinggiBadan;
    data['fakultas_jurusan'] = this.fakultasJurusan;
    data['nim'] = this.nim;
    data['nama_ayah'] = this.namaAyah;
    data['nama_ibu'] = this.namaIbu;
    data['status'] = this.status;
    data['berat_badan'] = this.beratBadan;
    return data;
  }
}