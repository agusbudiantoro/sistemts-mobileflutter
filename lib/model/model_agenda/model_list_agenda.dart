class ModelListAgenda {
  int? idAgenda;
  String? namaAgenda;
  int? bidang;
  String? fotoKegiatan;
  String? fotoPengurus;

  ModelListAgenda(
      {this.idAgenda,
      this.namaAgenda,
      this.bidang,
      this.fotoKegiatan,
      this.fotoPengurus});

  ModelListAgenda.fromJson(Map<String, dynamic> json) {
    idAgenda = json['id_agenda'];
    namaAgenda = json['nama_agenda'];
    bidang = json['bidang'];
    fotoKegiatan = json['foto_kegiatan'];
    fotoPengurus = json['foto_pengurus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_agenda'] = this.idAgenda;
    data['nama_agenda'] = this.namaAgenda;
    data['bidang'] = this.bidang;
    data['foto_kegiatan'] = this.fotoKegiatan;
    data['foto_pengurus'] = this.fotoPengurus;
    return data;
  }
}