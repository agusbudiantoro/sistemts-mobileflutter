class ModelListJadwal {
  int? idJadwal;
  String? waktu;
  String? tempat;

  ModelListJadwal({this.idJadwal, this.waktu, this.tempat});

  ModelListJadwal.fromJson(Map<String, dynamic> json) {
    idJadwal = json['id_jadwal'];
    waktu = json['waktu'];
    tempat = json['tempat'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_jadwal'] = this.idJadwal;
    data['waktu'] = this.waktu;
    data['tempat'] = this.tempat;
    return data;
  }
}