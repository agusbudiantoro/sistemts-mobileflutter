class ModelMessage {
  String? nama;
  String? pesan;
  String? jam;
  String? pengirim;
  String? penerima;

  ModelMessage({this.jam, this.nama, this.penerima, this.pengirim, this.pesan});

  ModelMessage.fromJson(Map<String, dynamic>json){
    jam = json['jam'];
    nama = json['nama'];
    penerima = json['penerima'];
    pengirim = json['pengirim'];
    pesan = json['pesan'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['jam']=this.jam;
    data['nama']=this.nama;
    data['pesan']=this.pesan;
    data['pengirim']=this.pengirim;
    data['penerima']=this.penerima;
    return data; 
  }
}