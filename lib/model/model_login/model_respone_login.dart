class ModelResponseLogin {
  String? token;
  String? email;
  int? id_pengguna;
  String? nama;
  int? role; 
  String? message;
  int? status;

  ModelResponseLogin({this.email, this.token, this.id_pengguna, this.nama, this.role, this.message, this.status});

  ModelResponseLogin.fromJson(Map<String, dynamic> json){
    token = json['token'];
    email = json['email'];
    id_pengguna = json['id_pengguna'];
    nama = json['nama'];
    role = json['role'];
    message = json['message'];
    status = json['status'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
      data['token'] = this.token;
      data['email'] = this.email;
      data['id_pengguna'] = this.id_pengguna;
      data['nama'] = this.nama;
      data['role'] = this.role;
      data['message'] = this.message;
      data['status'] = this.status;
      return data;
  }
}