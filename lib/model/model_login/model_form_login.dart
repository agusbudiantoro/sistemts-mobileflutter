class ModelFormLogin {
  String? email;
  String? password;

  ModelFormLogin({this.email,this.password});

  ModelFormLogin.fromJson(Map<String, dynamic> json){
    email = json['email'];
    password = json['password'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['password'] = this.password;
    return data;
  }
}