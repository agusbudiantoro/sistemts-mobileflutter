import 'dart:io';

import 'package:http/io_client.dart';
import 'package:sistemts/model/model_register/form_register_user.dart';
import 'package:sistemts/view_model/api/domain.dart';

const url = urlDomain+"/controller/api/v1/";

class RegisterApi {
  static Future<bool> registerAkun(data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    registUser convertData = data;
    Map<String, String>body={
      'email': convertData.email!,
      'password': convertData.password!,
      'nama_user':convertData.namaUser! 
    };
    try {
      print("cek");
      var response = await ioClient.post(Uri.parse(url+'registerAkun'), body: body);
      if(response.statusCode == 200){
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print(e);
      return throw Exception("Data tidak ditemukan");
    }
  }
}