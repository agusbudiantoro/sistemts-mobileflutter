import 'dart:convert';
import 'dart:io';

import 'package:http/io_client.dart';
import 'package:http/http.dart' as client;
import 'package:http_parser/http_parser.dart';
import 'package:path/path.dart';
import 'package:async/async.dart';
import 'package:sistemts/model/model_agenda/model_form_agenda.dart';
import 'package:sistemts/view_model/api/domain.dart';

const url = urlDomain+"/controller/api/v1/";

class AgendaApi {

  static Future<bool> deleteAgenda(int id)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      var response = await ioClient.delete(Uri.parse(url+'hapusAgenda/'+id.toString()));
      // print(response.body);
      if(response.statusCode == 200){
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return throw Exception(e.toString());
    }
  }

  static Future<bool> tambahAgenda(data)async{
    ModelFormAgenda myData = data;
    try {
    String fileName = basename(myData.fotoKegiatan!.path);
    String base64Image = base64Encode(myData.fotoKegiatan!.readAsBytesSync());
    String fileName1 = basename(myData.fotoPengurus!.path);
    String base64Image1 = base64Encode(myData.fotoPengurus!.readAsBytesSync());
    var stream = new client.ByteStream(DelegatingStream.typed(myData.fotoKegiatan!.openRead()));
    var length = await myData.fotoKegiatan!.length();
    var stream1 = new client.ByteStream(DelegatingStream.typed(myData.fotoPengurus!.openRead()));
    var length1 = await myData.fotoPengurus!.length();
    var request = client.MultipartRequest("POST", Uri.parse(url+'tambahAgenda'));
    request.fields["nama_agenda"] = myData.namaAgenda.toString();
    request.fields["bidang"] = myData.bidang.toString();
    request.headers.addAll({
      'Content-type': 'multipart/form-data',
      'Accept':'*/*'
    });
    var pic = await client.MultipartFile("foto_kegiatan", stream, length,filename: fileName, contentType: MediaType("multipart/form-data","multipart/form-data"));
    var pic1 = await client.MultipartFile("foto_pengurus", stream1, length1,filename: fileName1, contentType: MediaType("multipart/form-data","multipart/form-data"));
    request.files.add(pic);
    request.files.add(pic1);
    client.Response response = await client.Response.fromStream(await request.send());
    if(response.statusCode == 200){
      return true;
    }else {
      return false;
    }
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<dynamic> getAgendaByIdBidang(int id)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      var response = await ioClient.get(Uri.parse(url+'tampilAgendaByIdBidang/'+id.toString()));
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception(e.toString());
    }
  }
  
}