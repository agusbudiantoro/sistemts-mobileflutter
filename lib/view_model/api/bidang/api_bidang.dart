import 'dart:convert';
import 'dart:io';

import 'package:http/io_client.dart';
import 'package:http/http.dart' as client;
import 'package:http_parser/http_parser.dart';
import 'package:path/path.dart';
import 'package:async/async.dart';
import 'package:sistemts/view_model/api/domain.dart';

const url = urlDomain+"/controller/api/v1/";

class BidangApi {

  static Future<dynamic> getBidang()async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      var response = await ioClient.get(Uri.parse(url+'tampilBidang'));
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception(e.toString());
    }
  }
  
}