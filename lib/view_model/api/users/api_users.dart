import 'dart:convert';
import 'dart:io';

import 'package:http/io_client.dart';
import 'package:http/http.dart' as client;
import 'package:http_parser/http_parser.dart';
import 'package:path/path.dart';
import 'package:async/async.dart';
import 'package:sistemts/model/model_register/form_register_user.dart';
import 'package:sistemts/view_model/api/domain.dart';

const url = urlDomain+"/controller/api/v1/";

class UsersApi {

  static Future<dynamic> getUsersById(int id)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      var response = await ioClient.get(Uri.parse(url+'getUserById/'+id.toString()));
      print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception(e.toString());
    }
  }

  static Future<dynamic> getAllUsers()async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      var response = await ioClient.get(Uri.parse(url+'getAllUser'));
      print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception(e.toString());
    }
  }

  static Future<bool> putUser(data,id)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    int myId = id;
    registUser convertData = data;
    Map<String, String>body={
      'nim': convertData.nim!,
      'nama_user': convertData.namaUser!,
      'no_hp': convertData.noHp!,
      'tempat_tgl_lahir': convertData.tempatTglLahir!,
      'alamat': convertData.alamat!,
      'jenis_kelamin': convertData.jenisKelamin!,
      'agama': convertData.agama!,
      'tinggi_badan': convertData.tinggiBadan!,
      'fakultas_jurusan': convertData.fakultasJurusan!,
      'nama_ayah': convertData.namaAyah!,
      'nama_ibu': convertData.namaIbu!,
      'berat_badan': convertData.beratBadan!,
    };
    try {
      
      var response = await ioClient.put(Uri.parse(url+'updateUser/'+myId.toString()), body: body);
      print(response.body);
      if(response.statusCode == 200){
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return throw Exception(e.toString());
    }
  }
  
  static Future<bool> verifUser(id)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    int myId = id;
    try {
      
      var response = await ioClient.put(Uri.parse(url+'verifUser/'+myId.toString()));
      print(response.body);
      if(response.statusCode == 200){
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return throw Exception(e.toString());
    }
  }

}