import 'dart:convert';
import 'dart:io';

import 'package:http/io_client.dart';
import 'package:http/http.dart' as client;
import 'package:http_parser/http_parser.dart';
import 'package:path/path.dart';
import 'package:async/async.dart';
import 'package:sistemts/model/model_jadwal/model_list_jadwal.dart';
import 'package:sistemts/view_model/api/domain.dart';

const url = urlDomain+"/controller/api/v1/";

class JadwalApi {

  static Future<bool> hapusJadwal(id)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    int myid =id;
    try {
      
      var response = await ioClient.delete(Uri.parse(url+'deleteJadwal/'+myid.toString()));
      print(response.body);
      if(response.statusCode == 200){
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return throw Exception(e.toString());
    }
  }

  static Future<bool> tambahJadwal(data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    ModelListJadwal convertData = data;
    Map<String, String>body={
      'waktu': convertData.waktu!,
      'tempat': convertData.tempat!
    };
    try {
      
      var response = await ioClient.post(Uri.parse(url+'tambahJadwal'), body: body);
      print(response.body);
      if(response.statusCode == 200){
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return throw Exception(e.toString());
    }
  }

  static Future<dynamic> getJadwal()async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      var response = await ioClient.get(Uri.parse(url+'tampilJadwal'));
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception(e.toString());
    }
  }
  
}