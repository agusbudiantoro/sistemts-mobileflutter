import 'dart:convert';
import 'dart:io';

import 'package:http/io_client.dart';
import 'package:http/http.dart' as client;
import 'package:http_parser/http_parser.dart';
import 'package:path/path.dart';
import 'package:async/async.dart';
import 'package:sistemts/model/model_penerimaan_kader/model_form.dart';
import 'package:sistemts/view_model/api/domain.dart';

const url = urlDomain+"/controller/api/v1/";

class PenerimaanKaderApi {

  static Future<bool> verifPenerimaanKader(id_kader,token)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      var response = await ioClient.put(Uri.parse(url+'editStatusKader/'+id_kader.toString()));
      // print(response.body);
      if(response.statusCode == 200){
        return true;
      } else {
        return false;
      }
      // return response.body;
    } catch (e) {
      return throw Exception(e.toString());
    }
  }

  static Future<dynamic> getAllPenerimaanKader(token)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      var response = await ioClient.get(Uri.parse(url+'getAllKader'));
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception(e.toString());
    }
  }

  static Future<bool> deletePenerimaanKader(id_kader,token)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      var response = await ioClient.delete(Uri.parse(url+'deleteKader/'+id_kader.toString()));
      // print(response.body);
      if(response.statusCode == 200){
        return true;
      } else {
        return false;
      }
      // return response.body;
    } catch (e) {
      return throw Exception(e.toString());
    }
  }

  static Future<dynamic> getPenerimaanKader(id_user,token)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      var response = await ioClient.get(Uri.parse(url+'getKaderByIdUser/'+id_user.toString()));
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception(e.toString());
    }
  }
  
  static Future<bool> tambahKader(id,token,data)async{
    ModelFormPenerimaanKader myData = data;
    int myId = id;
    try {
    String fileName = basename(myData.file!.path);
    String base64Image = base64Encode(myData.file!.readAsBytesSync());
    var stream = new client.ByteStream(DelegatingStream.typed(myData.file!.openRead()));
    var length = await myData.file!.length();
    var request = client.MultipartRequest("POST", Uri.parse(url+'tambahKader'));
    request.fields["id_user"] = id.toString();
    request.headers.addAll({
      'Content-type': 'multipart/form-data',
      'Accept':'*/*'
    });
    var pic = await client.MultipartFile("file", stream, length,filename: fileName, contentType: MediaType("multipart/form-data","multipart/form-data"));
    request.files.add(pic);
    client.Response response = await client.Response.fromStream(await request.send());
    if(response.statusCode == 200){
      return true;
    }else {
      return false;
    }
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }
  
}