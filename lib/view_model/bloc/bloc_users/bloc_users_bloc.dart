import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sistemts/model/model_register/form_register_user.dart';
import 'package:sistemts/model/model_users/model_list_user.dart';
import 'package:sistemts/view_model/api/users/api_users.dart';

part 'bloc_users_event.dart';
part 'bloc_users_state.dart';

class BlocUsersBloc extends Bloc<BlocUsersEvent, BlocUsersState> {
  BlocUsersBloc() : super(BlocUsersInitial());

  @override
  Stream<BlocUsersState> mapEventToState(
    BlocUsersEvent event,
  ) async* {
    if(event is EventGetUsersById){
      print("cek");
      yield* _getUsersById(event.id!);
    }
    if(event is EventGetAllUser){
      print("cek");
      yield* _getAllUsers();
    }
    if(event is EventPutUser){
      print("cek");
      yield* _putUser(event.data!);
    }
    if(event is EventVerifUser){
      print("cek");
      yield* _verifUser(event.id!);
    }
  }
}

Stream<BlocUsersState> _verifUser(int id)async*{
  yield BlocVerifUsersLoadingState();
  try {
    bool resdata = await UsersApi.verifUser(id);
    if(resdata == true){
      yield BlocVerifUsersSuccessState(); 
    }else {
      yield BlocVerifUsersFailedState();
    }
  } catch (e) {
    yield BlocVerifUsersFailedState(errorMessage: e.toString());
  }
}

Stream<BlocUsersState> _getAllUsers()async*{
  yield BlocGetAllUsersLoadingState();
  try {
    dynamic data = await UsersApi.getAllUsers();
    // print(data);
    var listData = jsonDecode(data) as List;
    // print(listData);
    var hasilFinal = listData.map<ModelListUser>((item) => ModelListUser.fromJson(item)).toList();
    List<ModelListUser> hasilConvert=hasilFinal;
    yield BlocGetAllUsersSuccessState(listData: hasilConvert); 
  } catch (e) {
    yield BlocGetAllUsersFailedState(errorMessage: e.toString());
  }
}

Stream<BlocUsersState> _putUser(registUser data)async*{
  yield BlocPutUsersLoadingState();
  try {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int? id_pengguna = await preferences.getInt("id_pengguna");
    bool resdata = await UsersApi.putUser(data, id_pengguna!);
    if(resdata == true){
      yield BlocPutUsersSuccessState(); 
    }else {
      yield BlocPutUsersFailedState();
    }
  } catch (e) {
    yield BlocPutUsersFailedState(errorMessage: e.toString());
  }
}

Stream<BlocUsersState> _getUsersById(int id)async*{
  yield BlocGetUsersLoadingState();
  try {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int? id_pengguna;
    int? role = await preferences.getInt("role");
    if(role == 2){
      id_pengguna = await preferences.getInt("id_pengguna");
    } else {
      id_pengguna = await id;
    }
    dynamic data = await UsersApi.getUsersById(id_pengguna!);
    // print(data);
    var listData = jsonDecode(data) as List;
    // print(listData);
    var hasilFinal = listData.map<ModelListUser>((item) => ModelListUser.fromJson(item)).toList();
    List<ModelListUser> hasilConvert=hasilFinal;
    yield BlocGetUsersSuccessState(listData: hasilConvert); 
  } catch (e) {
    yield BlocGetUsersFailedState(errorMessage: e.toString());
  }
}