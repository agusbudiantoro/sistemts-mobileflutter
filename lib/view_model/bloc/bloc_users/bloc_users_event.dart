part of 'bloc_users_bloc.dart';

@immutable
abstract class BlocUsersEvent {}

class EventGetUsersById extends BlocUsersEvent {
  final int? id;
  EventGetUsersById({this.id});
}

class EventPutUser extends BlocUsersEvent {
  final registUser? data;
  EventPutUser({this.data});
}

class EventGetAllUser extends BlocUsersEvent {
  
}

class EventVerifUser extends BlocUsersEvent {
  final int? id;
  EventVerifUser({this.id});
}