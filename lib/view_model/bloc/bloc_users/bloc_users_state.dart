part of 'bloc_users_bloc.dart';

@immutable
abstract class BlocUsersState {}

class BlocUsersInitial extends BlocUsersState {}

//get user by iq
class BlocGetUsersSuccessState extends BlocUsersState {
  final List<ModelListUser>? listData;
  BlocGetUsersSuccessState({this.listData}); 
}

class BlocGetUsersLoadingState extends BlocUsersState {
}

class BlocGetUsersFailedState extends BlocUsersState {
  final String? errorMessage;
  BlocGetUsersFailedState({this.errorMessage});
}

// edit user
class BlocPutUsersSuccessState extends BlocUsersState {
  final bool? status;
  BlocPutUsersSuccessState({this.status}); 
}

class BlocPutUsersLoadingState extends BlocUsersState {
}

class BlocPutUsersFailedState extends BlocUsersState {
  final String? errorMessage;
  BlocPutUsersFailedState({this.errorMessage});
}

// get all user
class BlocGetAllUsersSuccessState extends BlocUsersState {
  final List<ModelListUser>? listData;
  BlocGetAllUsersSuccessState({this.listData}); 
}

class BlocGetAllUsersLoadingState extends BlocUsersState {
}

class BlocGetAllUsersFailedState extends BlocUsersState {
  final String? errorMessage;
  BlocGetAllUsersFailedState({this.errorMessage});
}

//verif user
class BlocVerifUsersSuccessState extends BlocUsersState {
  final List<ModelListUser>? listData;
  BlocVerifUsersSuccessState({this.listData}); 
}

class BlocVerifUsersLoadingState extends BlocUsersState {
}

class BlocVerifUsersFailedState extends BlocUsersState {
  final String? errorMessage;
  BlocVerifUsersFailedState({this.errorMessage});
}