part of 'biodata_kejuaraan_bloc.dart';

@immutable
abstract class BiodataKejuaraanEvent {}

class GetBiodataKejuaraanByIdUser extends BiodataKejuaraanEvent{
  final ModelFormBiodataKejuaraan? myForm;
  GetBiodataKejuaraanByIdUser({this.myForm});
}

class PostBiodataKejuaraan extends BiodataKejuaraanEvent{
  final ModelFormBiodataKejuaraan? myForm;
  PostBiodataKejuaraan({this.myForm});
}

class DeleteBiodataKejuaraan extends BiodataKejuaraanEvent{
  final ModelFormBiodataKejuaraan? myForm;
  DeleteBiodataKejuaraan({this.myForm});
}

class GetAllBiodataKejuaraan extends BiodataKejuaraanEvent{
  final ModelFormBiodataKejuaraan? myForm;
  GetAllBiodataKejuaraan({this.myForm});
}

class VerifyBiodataKejuaraan extends BiodataKejuaraanEvent {
  final int? id;
  VerifyBiodataKejuaraan({this.id});
}