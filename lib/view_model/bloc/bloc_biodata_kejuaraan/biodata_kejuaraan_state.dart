part of 'biodata_kejuaraan_bloc.dart';

@immutable
abstract class BiodataKejuaraanState {}

class BiodataKejuaraanInitial extends BiodataKejuaraanState {}

class BlocStateSuksesGetBiodataKejuaraan extends BiodataKejuaraanState{
  final List<ModelListBiodataKejuaraan>? myData;
    BlocStateSuksesGetBiodataKejuaraan({this.myData});
}

class BlocaStateFailedGetBiodataKejuaraan extends BiodataKejuaraanState{
  final String? errorMessage;
  BlocaStateFailedGetBiodataKejuaraan({this.errorMessage});
}

class BlocStateLoadingGetBiodataKejuaraan extends BiodataKejuaraanState{

}

class BlocStateSuksesPostBiodataKejuaraan extends BiodataKejuaraanState{
  final ModelListBiodataKejuaraan? myData;
    BlocStateSuksesPostBiodataKejuaraan({this.myData});
}

class BlocaStateFailedPostBiodataKejuaraan extends BiodataKejuaraanState{
  final String? errorMessage;
  BlocaStateFailedPostBiodataKejuaraan({this.errorMessage});
}

class BlocStateLoadingPostBiodataKejuaraan extends BiodataKejuaraanState{

}

class BlocStateSuksesDeleteBiodataKejuaraan extends BiodataKejuaraanState{
  final bool? status;
    BlocStateSuksesDeleteBiodataKejuaraan({this.status});
}

class BlocaStateFailedDeleteBiodataKejuaraan extends BiodataKejuaraanState{
  final String? errorMessage;
  BlocaStateFailedDeleteBiodataKejuaraan({this.errorMessage});
}

class BlocStateLoadingDeleteBiodataKejuaraan extends BiodataKejuaraanState{

}

class BlocStateSuksesGetAllBiodataKejuaraan extends BiodataKejuaraanState{
  final List<ModelListBiodataKejuaraan>? myData;
    BlocStateSuksesGetAllBiodataKejuaraan({this.myData});
}

class BlocaStateFailedGetAllBiodataKejuaraan extends BiodataKejuaraanState{
  final String? errorMessage;
  BlocaStateFailedGetAllBiodataKejuaraan({this.errorMessage});
}

class BlocStateLoadingGetAllBiodataKejuaraan extends BiodataKejuaraanState{
}

class BlocStateSuksesVerifBiodataKejuaraan extends BiodataKejuaraanState{
  final bool? status;
    BlocStateSuksesVerifBiodataKejuaraan({this.status});
}

class BlocaStateFailedVerifBiodataKejuaraan extends BiodataKejuaraanState{
  final String? errorMessage;
  BlocaStateFailedVerifBiodataKejuaraan({this.errorMessage});
}

class BlocStateLoadingVerifBiodataKejuaraan extends BiodataKejuaraanState{

}