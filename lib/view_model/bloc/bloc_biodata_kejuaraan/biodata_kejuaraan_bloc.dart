import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sistemts/model/model_biodata_kejuaraan/model_form_biodata_kejuaraan.dart';
import 'package:sistemts/model/model_biodata_kejuaraan/model_list_biodata_kejuaraan.dart';
import 'package:sistemts/view_model/api/biodata_kejuaraan/api_biodata_kejuaraan.dart';

part 'biodata_kejuaraan_event.dart';
part 'biodata_kejuaraan_state.dart';

class BiodataKejuaraanBloc extends Bloc<BiodataKejuaraanEvent, BiodataKejuaraanState> {
  BiodataKejuaraanBloc() : super(BiodataKejuaraanInitial());

    @override
  Stream<BiodataKejuaraanState> mapEventToState(
    BiodataKejuaraanEvent event,
  ) async* {
    if(event is GetBiodataKejuaraanByIdUser){
      print("hit");
      yield* _getBiodataKejuaraanByIdUser();
    }
    if(event is GetAllBiodataKejuaraan){
      print("hit");
      yield* _getAllBiodata();
    }
    if(event is PostBiodataKejuaraan){
      print("cek sini");
      yield* _postBiodata(event.myForm);
    }
    if(event is DeleteBiodataKejuaraan){
      yield* _deleteBiodata(event.myForm);
    }
    if(event is VerifyBiodataKejuaraan){
      yield* _verifBiodata(event.id!);
    }
  }
}

Stream<BiodataKejuaraanState> _verifBiodata(int id)async*{
  print("sini");
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield BlocStateLoadingDeleteBiodataKejuaraan();
  try {
    String? token = await preferences.getString("token");
      bool value = await BiodataKejuaraanApi.verifBiodataKejuaraan(id,token);
      if(value == true){
        yield BlocStateSuksesVerifBiodataKejuaraan(status: value);
      } else {
        yield BlocaStateFailedVerifBiodataKejuaraan();
      }
  } catch (e) {
    print(e.toString());
    yield BlocaStateFailedVerifBiodataKejuaraan();
  }
}

Stream<BiodataKejuaraanState> _getAllBiodata()async*{
  print("sini");
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield BlocStateLoadingGetAllBiodataKejuaraan();
  try {
    String? token = await preferences.getString("token");
    int? id_pengguna = await preferences.getInt("id_pengguna");
    if(token != null){
      dynamic value = await BiodataKejuaraanApi.getAllBiodataKejuaraan();
     var hasil = jsonDecode(value) as List;
      print(hasil);
      var hasilFinal = hasil
            .map<ModelListBiodataKejuaraan>((json) => ModelListBiodataKejuaraan.fromJson(json))
            .toList();
      List<ModelListBiodataKejuaraan> hasilConvert = hasilFinal;
      yield BlocStateSuksesGetAllBiodataKejuaraan(myData: hasilConvert);
    } else {
      yield BlocaStateFailedGetAllBiodataKejuaraan();
    }
  } catch (e) {
    print(e.toString());
    yield BlocaStateFailedGetAllBiodataKejuaraan();
  }
}

Stream<BiodataKejuaraanState> _deleteBiodata(data)async*{
  print("sini");
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield BlocStateLoadingDeleteBiodataKejuaraan();
  try {
    ModelFormBiodataKejuaraan myForm = await data;
    String? token = await preferences.getString("token");
      bool value = await BiodataKejuaraanApi.deleteBiodataKejuaraan(myForm.id_kejuaraan,token);
      if(value == true){
        yield BlocStateSuksesDeleteBiodataKejuaraan(status: value);
      } else {
        yield BlocaStateFailedDeleteBiodataKejuaraan();
      }
  } catch (e) {
    print(e.toString());
    yield BlocaStateFailedDeleteBiodataKejuaraan();
  }
}

Stream<BiodataKejuaraanState> _getBiodataKejuaraanByIdUser()async*{
  print("sini");
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield BlocStateLoadingGetBiodataKejuaraan();
  try {
    String? token = await preferences.getString("token");
    int? id_pengguna = await preferences.getInt("id_pengguna");
    if(token != null){
      dynamic value = await BiodataKejuaraanApi.getBiodataKejuaraan(id_pengguna,token);
     var hasil = jsonDecode(value) as List;
      print(hasil);
      var hasilFinal = hasil
            .map<ModelListBiodataKejuaraan>((json) => ModelListBiodataKejuaraan.fromJson(json))
            .toList();
      List<ModelListBiodataKejuaraan> hasilConvert = hasilFinal;
      yield BlocStateSuksesGetBiodataKejuaraan(myData: hasilConvert);
    } else {
      yield BlocaStateFailedGetBiodataKejuaraan();
    }
  } catch (e) {
    print(e.toString());
    yield BlocaStateFailedGetBiodataKejuaraan();
  }
}

Stream<BiodataKejuaraanState> _postBiodata(form)async*{
  print("sini");
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield BlocStateLoadingPostBiodataKejuaraan();
  try {
    String? token = await preferences.getString("token");
    int? id_pengguna = await preferences.getInt("id_pengguna");
    if(token != null){
      bool value = await BiodataKejuaraanApi.tambahBiodata(id_pengguna,token,form);
      yield BlocStateSuksesPostBiodataKejuaraan();
    } else {
      yield BlocaStateFailedPostBiodataKejuaraan();
    }
  } catch (e) {
    print(e.toString());
    yield BlocaStateFailedPostBiodataKejuaraan();
  }
}
