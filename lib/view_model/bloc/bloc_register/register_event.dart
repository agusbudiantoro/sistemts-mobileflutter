part of 'register_bloc.dart';

@immutable
abstract class RegisterEvent {}

class BlocEventRegister extends RegisterEvent{
  final registUser? myData;
  BlocEventRegister({@required this.myData});
}
