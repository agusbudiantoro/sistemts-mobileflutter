import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sistemts/model/model_agenda/model_form_agenda.dart';
import 'package:sistemts/model/model_agenda/model_list_agenda.dart';
import 'package:sistemts/view_model/api/agenda/api_agenda.dart';

part 'bloc_agenda_event.dart';
part 'bloc_agenda_state.dart';

class BlocAgendaBloc extends Bloc<BlocAgendaEvent, BlocAgendaState> {
  BlocAgendaBloc() : super(BlocAgendaInitial());

  @override
  Stream<BlocAgendaState> mapEventToState(
    BlocAgendaEvent event,
  ) async* {
    if(event is EventGetAgendaByIdBidang){
      print("cek");
      yield* _getAgendaByIdBidang(event.id!);
    }
    if(event is EventPostAgenda){
      print("cek");
      yield* _postAgenda(event.data!);
    }
    if(event is EventDeleteAgenda){
      print("cek");
      yield* _deleteAgenda(event.id!);
    }
  }
}

Stream<BlocAgendaState> _deleteAgenda(int id)async*{
  yield BlocDeleteAgendaLoadingState();
  try {
    bool resdata = await AgendaApi.deleteAgenda(id);
    if(resdata == true){
      yield BlocDeleteAgendaSuccessState(); 
    }else {
      yield BlocDeleteAgendaFailedState();
    }
  } catch (e) {
    yield BlocDeleteAgendaFailedState(errorMessage: e.toString());
  }
}

Stream<BlocAgendaState> _postAgenda(ModelFormAgenda data)async*{
  yield BlocPostAgendaLoadingState();
  try {
    bool resdata = await AgendaApi.tambahAgenda(data);
    if(resdata == true){
      yield BlocPostAgendaSuccessState(); 
    }else {
      yield BlocPostAgendaFailedState();
    }
  } catch (e) {
    yield BlocPostAgendaFailedState(errorMessage: e.toString());
  }
}

Stream<BlocAgendaState> _getAgendaByIdBidang(int id)async*{
  yield BlocGetAgendaLoadingState();
  try {
    dynamic data = await AgendaApi.getAgendaByIdBidang(id);
    // print(data);
    var listData = jsonDecode(data) as List;
    // print(listData);
    var hasilFinal = listData.map<ModelListAgenda>((item) => ModelListAgenda.fromJson(item)).toList();
    List<ModelListAgenda> hasilConvert=hasilFinal;
    yield BlocGetAgendaSuccessState(listData: hasilConvert); 
  } catch (e) {
    BlocGetAgendaFailedState(errorMessage: e.toString());
  }
}