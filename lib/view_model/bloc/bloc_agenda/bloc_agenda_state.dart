part of 'bloc_agenda_bloc.dart';

@immutable
abstract class BlocAgendaState {}

class BlocAgendaInitial extends BlocAgendaState {}

class BlocGetAgendaSuccessState extends BlocAgendaState {
  final List<ModelListAgenda>? listData;
  BlocGetAgendaSuccessState({this.listData}); 
}

class BlocGetAgendaLoadingState extends BlocAgendaState {
}

class BlocGetAgendaFailedState extends BlocAgendaState {
  final String? errorMessage;
  BlocGetAgendaFailedState({this.errorMessage});
}

class BlocPostAgendaSuccessState extends BlocAgendaState {
  final List<ModelListAgenda>? listData;
  BlocPostAgendaSuccessState({this.listData}); 
}

class BlocPostAgendaLoadingState extends BlocAgendaState {
}

class BlocPostAgendaFailedState extends BlocAgendaState {
  final String? errorMessage;
  BlocPostAgendaFailedState({this.errorMessage});
}

class BlocDeleteAgendaSuccessState extends BlocAgendaState {
  final bool? status;
  BlocDeleteAgendaSuccessState({this.status}); 
}

class BlocDeleteAgendaLoadingState extends BlocAgendaState {
}

class BlocDeleteAgendaFailedState extends BlocAgendaState {
  final String? errorMessage;
  BlocDeleteAgendaFailedState({this.errorMessage});
}