part of 'bloc_agenda_bloc.dart';

@immutable
abstract class BlocAgendaEvent {}

class EventGetAgendaByIdBidang extends BlocAgendaEvent {
  final int? id;
  EventGetAgendaByIdBidang({this.id}); 
}

class EventPostAgenda extends BlocAgendaEvent {
  final ModelFormAgenda? data;
  EventPostAgenda({this.data});
}

class EventDeleteAgenda extends BlocAgendaEvent {
  final int? id;
  EventDeleteAgenda({this.id});
}