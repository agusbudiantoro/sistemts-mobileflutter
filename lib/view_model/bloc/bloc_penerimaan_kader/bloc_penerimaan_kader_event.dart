part of 'bloc_penerimaan_kader_bloc.dart';

@immutable
abstract class BlocPenerimaanKaderEvent {}

class GetPenerimaanKaderByIdUser extends BlocPenerimaanKaderEvent{
  final ModelFormPenerimaanKader? myForm;
  GetPenerimaanKaderByIdUser({this.myForm});
}

class PostPenerimaanKader extends BlocPenerimaanKaderEvent{
  final ModelFormPenerimaanKader? myForm;
  PostPenerimaanKader({this.myForm});
}

class DeletePenerimaanKader extends BlocPenerimaanKaderEvent{
  final ModelFormPenerimaanKader? myForm;
  DeletePenerimaanKader({this.myForm});
}

class GetAllPenerimaanKader extends BlocPenerimaanKaderEvent{
  final ModelFormPenerimaanKader? myForm;
  GetAllPenerimaanKader({this.myForm});
}

class VerifPenerimaanKader extends BlocPenerimaanKaderEvent{
  final int? id;
  VerifPenerimaanKader({this.id});
}