part of 'bloc_penerimaan_kader_bloc.dart';

@immutable
abstract class BlocPenerimaanKaderState {}

class BlocPenerimaanKaderInitial extends BlocPenerimaanKaderState {}

class BlocStateSuksesGetPenerimaanKader extends BlocPenerimaanKaderState{
  final List<ModelListPenerimaanKader>? myData;
    BlocStateSuksesGetPenerimaanKader({this.myData});
}

class BlocaStateFailedGetPenerimaanKader extends BlocPenerimaanKaderState{
  final String? errorMessage;
  BlocaStateFailedGetPenerimaanKader({this.errorMessage});
}

class BlocStateLoadingGetPenerimaanKader extends BlocPenerimaanKaderState{

}

class BlocStateSuksesPostPenerimaanKader extends BlocPenerimaanKaderState{
  final ModelListPenerimaanKader? myData;
    BlocStateSuksesPostPenerimaanKader({this.myData});
}

class BlocaStateFailedPostPenerimaanKader extends BlocPenerimaanKaderState{
  final String? errorMessage;
  BlocaStateFailedPostPenerimaanKader({this.errorMessage});
}

class BlocStateLoadingPostPenerimaanKader extends BlocPenerimaanKaderState{

}

class BlocStateSuksesDeletePenerimaanKader extends BlocPenerimaanKaderState{
  final bool? status;
    BlocStateSuksesDeletePenerimaanKader({this.status});
}

class BlocaStateFailedDeletePenerimaanKader extends BlocPenerimaanKaderState{
  final String? errorMessage;
  BlocaStateFailedDeletePenerimaanKader({this.errorMessage});
}

class BlocStateLoadingDeletePenerimaanKader extends BlocPenerimaanKaderState{

}

class BlocStateSuksesGetAllPenerimaanKader extends BlocPenerimaanKaderState{
  final List<ModelListPenerimaanKader>? myData;
    BlocStateSuksesGetAllPenerimaanKader({this.myData});
}

class BlocaStateFailedGetAllPenerimaanKader extends BlocPenerimaanKaderState{
  final String? errorMessage;
  BlocaStateFailedGetAllPenerimaanKader({this.errorMessage});
}

class BlocStateLoadingGetAllPenerimaanKader extends BlocPenerimaanKaderState{

}

class BlocStateSuksesVerifPenerimaanKader extends BlocPenerimaanKaderState{
  final bool? status;
    BlocStateSuksesVerifPenerimaanKader({this.status});
}

class BlocaStateFailedVerifPenerimaanKader extends BlocPenerimaanKaderState{
  final String? errorMessage;
  BlocaStateFailedVerifPenerimaanKader({this.errorMessage});
}

class BlocStateLoadingVerifPenerimaanKader extends BlocPenerimaanKaderState{

}