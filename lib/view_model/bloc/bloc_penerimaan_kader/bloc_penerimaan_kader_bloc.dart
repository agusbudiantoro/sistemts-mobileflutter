import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sistemts/model/model_penerimaan_kader/model_form.dart';
import 'package:sistemts/model/model_penerimaan_kader/model_list.dart';
import 'package:sistemts/view_model/api/penerimaan_kader/api_penerimaan_kader.dart';

part 'bloc_penerimaan_kader_event.dart';
part 'bloc_penerimaan_kader_state.dart';

class BlocPenerimaanKaderBloc extends Bloc<BlocPenerimaanKaderEvent, BlocPenerimaanKaderState> {
  BlocPenerimaanKaderBloc() : super(BlocPenerimaanKaderInitial());
  
  @override
  Stream<BlocPenerimaanKaderState> mapEventToState(
    BlocPenerimaanKaderEvent event,
  ) async* {
    if(event is GetPenerimaanKaderByIdUser){
      print("hit");
      yield* _getPenerimaanKaderByIdUser();
    }
    if(event is PostPenerimaanKader){
      print("cek sini");
      yield* _postKader(event.myForm);
    }
    if(event is DeletePenerimaanKader){
      yield* _deleteKader(event.myForm);
    }
    if(event is GetAllPenerimaanKader){
      print("hit");
      yield* _getAllPenerimaanKader();
    }
    if(event is VerifPenerimaanKader){
      yield* _verifKader(event.id!);
    }
  }
}

Stream<BlocPenerimaanKaderState> _verifKader(int id)async*{
  print("sini");
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield BlocStateLoadingVerifPenerimaanKader();
  try {
    String? token = await preferences.getString("token");
      bool value = await PenerimaanKaderApi.verifPenerimaanKader(id,token);
      if(value == true){
        yield BlocStateSuksesVerifPenerimaanKader(status: value);
      } else {
        yield BlocaStateFailedVerifPenerimaanKader();
      }
  } catch (e) {
    print(e.toString());
    yield BlocaStateFailedVerifPenerimaanKader();
  }
}

Stream<BlocPenerimaanKaderState> _getAllPenerimaanKader()async*{
  print("sini");
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield BlocStateLoadingGetAllPenerimaanKader();
  try {
    String? token = await preferences.getString("token");
    if(token != null){
      dynamic value = await PenerimaanKaderApi.getAllPenerimaanKader(token);
     var hasil = jsonDecode(value) as List;
      print(hasil);
      var hasilFinal = hasil
            .map<ModelListPenerimaanKader>((json) => ModelListPenerimaanKader.fromJson(json))
            .toList();
      List<ModelListPenerimaanKader> hasilConvert = hasilFinal;
      yield BlocStateSuksesGetAllPenerimaanKader(myData: hasilConvert);
    } else {
      yield BlocaStateFailedGetAllPenerimaanKader();
    }
  } catch (e) {
    print(e.toString());
    yield BlocaStateFailedGetAllPenerimaanKader();
  }
}


Stream<BlocPenerimaanKaderState> _deleteKader(data)async*{
  print("sini");
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield BlocStateLoadingDeletePenerimaanKader();
  try {
    ModelFormPenerimaanKader myForm = await data;
    String? token = await preferences.getString("token");
      bool value = await PenerimaanKaderApi.deletePenerimaanKader(myForm.id_kader,token);
      if(value == true){
        yield BlocStateSuksesDeletePenerimaanKader(status: value);
      } else {
        yield BlocaStateFailedDeletePenerimaanKader();
      }
  } catch (e) {
    print(e.toString());
    yield BlocaStateFailedDeletePenerimaanKader();
  }
}

Stream<BlocPenerimaanKaderState> _getPenerimaanKaderByIdUser()async*{
  print("sini");
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield BlocStateLoadingGetPenerimaanKader();
  try {
    String? token = await preferences.getString("token");
    int? id_pengguna = await preferences.getInt("id_pengguna");
    if(token != null){
      dynamic value = await PenerimaanKaderApi.getPenerimaanKader(id_pengguna,token);
     var hasil = jsonDecode(value) as List;
      print(hasil);
      var hasilFinal = hasil
            .map<ModelListPenerimaanKader>((json) => ModelListPenerimaanKader.fromJson(json))
            .toList();
      List<ModelListPenerimaanKader> hasilConvert = hasilFinal;
      yield BlocStateSuksesGetPenerimaanKader(myData: hasilConvert);
    } else {
      yield BlocaStateFailedGetPenerimaanKader();
    }
  } catch (e) {
    print(e.toString());
    yield BlocaStateFailedGetPenerimaanKader();
  }
}

Stream<BlocPenerimaanKaderState> _postKader(form)async*{
  print("sini");
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield BlocStateLoadingPostPenerimaanKader();
  try {
    String? token = await preferences.getString("token");
    int? id_pengguna = await preferences.getInt("id_pengguna");
    if(token != null){
      bool value = await PenerimaanKaderApi.tambahKader(id_pengguna,token,form);
      yield BlocStateSuksesPostPenerimaanKader();
    } else {
      yield BlocaStateFailedPostPenerimaanKader();
    }
  } catch (e) {
    print(e.toString());
    yield BlocaStateFailedPostPenerimaanKader();
  }
}
