part of 'bloc_jadwal_bloc.dart';

@immutable
abstract class BlocJadwalState {}

class BlocJadwalInitial extends BlocJadwalState {}

class BlocGetJadwalSuccessState extends BlocJadwalState {
  final List<ModelListJadwal>? listData;
  BlocGetJadwalSuccessState({this.listData}); 
}

class BlocGetJadwalLoadingState extends BlocJadwalState {
}

class BlocGetJadwalFailedState extends BlocJadwalState {
  final String? errorMessage;
  BlocGetJadwalFailedState({this.errorMessage});
}

class BlocPostJadwalSuccessState extends BlocJadwalState {
  final bool? status;
  BlocPostJadwalSuccessState({this.status}); 
}

class BlocPostJadwalLoadingState extends BlocJadwalState {
}

class BlocPostJadwalFailedState extends BlocJadwalState {
  final String? errorMessage;
  BlocPostJadwalFailedState({this.errorMessage});
}

class BlocDeleteJadwalSuccessState extends BlocJadwalState {
  final bool? status;
  BlocDeleteJadwalSuccessState({this.status}); 
}

class BlocDeleteJadwalLoadingState extends BlocJadwalState {
}

class BlocDeleteJadwalFailedState extends BlocJadwalState {
  final String? errorMessage;
  BlocDeleteJadwalFailedState({this.errorMessage});
}