import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sistemts/model/model_jadwal/model_list_jadwal.dart';
import 'package:sistemts/view_model/api/jadwal/api_jadwal.dart';

part 'bloc_jadwal_event.dart';
part 'bloc_jadwal_state.dart';

class BlocJadwalBloc extends Bloc<BlocJadwalEvent, BlocJadwalState> {
  BlocJadwalBloc() : super(BlocJadwalInitial());

  @override
  Stream<BlocJadwalState> mapEventToState(
    BlocJadwalEvent event,
  ) async* {
    if(event is EventGetJadwal){
      print("cek");
      yield* _getJadwal();
    }
    if(event is EventPostJadwal){
      print("cek");
      yield* _postJadwal(event.data!);
    }
    if(event is EventDeleteJadwal){
      print("cek");
      yield* _delJadwal(event.id!);
    }
  }
}

Stream<BlocJadwalState> _delJadwal(int id)async*{
  yield BlocDeleteJadwalLoadingState();
  try {
    bool resdata = await JadwalApi.hapusJadwal(id);
    print(resdata);
    if(resdata == true){
      print("sukses");
      yield BlocDeleteJadwalSuccessState(status: true);
    }else {
      print("fail");
      yield BlocDeleteJadwalFailedState();
    }
  } catch (e) {
    yield BlocDeleteJadwalFailedState(errorMessage: e.toString());
  }
}

Stream<BlocJadwalState> _postJadwal(ModelListJadwal data)async*{
  yield BlocPostJadwalLoadingState();
  try {
    bool resdata = await JadwalApi.tambahJadwal(data);
    if(resdata == true){
      yield BlocPostJadwalSuccessState(status: true);
    }else {
      yield BlocPostJadwalFailedState();
    }
  } catch (e) {
    yield BlocPostJadwalFailedState(errorMessage: e.toString());
  }
}

Stream<BlocJadwalState> _getJadwal()async*{
  yield BlocGetJadwalLoadingState();
  try {
    dynamic data = await JadwalApi.getJadwal();
    // print(data);
    var listData = jsonDecode(data) as List;
    // print(listData);
    var hasilFinal = listData.map<ModelListJadwal>((item) => ModelListJadwal.fromJson(item)).toList();
    List<ModelListJadwal> hasilConvert=hasilFinal;
    yield BlocGetJadwalSuccessState(listData: hasilConvert); 
  } catch (e) {
    BlocGetJadwalFailedState(errorMessage: e.toString());
  }
}