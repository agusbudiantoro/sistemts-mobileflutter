part of 'bloc_jadwal_bloc.dart';

@immutable
abstract class BlocJadwalEvent {}

class EventGetJadwal extends BlocJadwalEvent {
  
}

class EventPostJadwal extends BlocJadwalEvent {
  final ModelListJadwal? data;
  EventPostJadwal({this.data});
}

class EventDeleteJadwal extends BlocJadwalEvent {
  final int? id;
  EventDeleteJadwal({this.id});
}