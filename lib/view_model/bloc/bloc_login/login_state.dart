part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}

class BlocLoginInitial extends LoginState {}

class BlocStateSukses extends LoginState{
  final ModelResponseLogin? myData;
    BlocStateSukses({this.myData});
}

class BlocaStateFailed extends LoginState{
  final String? errorMessage;
  BlocaStateFailed({this.errorMessage});
}

class BlocStateLoading extends LoginState{

}


class BlocStateCekTokenSukses extends LoginState{
  final ModelResponseLogin? data;
  BlocStateCekTokenSukses({this.data});
}

class BlocaStateCekTokenFailed extends LoginState{
  final String? errorMessage;
  BlocaStateCekTokenFailed({this.errorMessage});
}