part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class BlocEventLogin extends LoginEvent{
  final ModelFormLogin? myData;
  BlocEventLogin({this.myData});
}

class BlocCekToken extends LoginEvent {
  final String? token;
  BlocCekToken({this.token});
}