import 'dart:convert';

import 'package:sistemts/model/model_login/model_form_login.dart';
import 'package:sistemts/model/model_login/model_respone_login.dart';
import 'package:sistemts/view_model/api/login/api_login.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginInitial());

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if(event is BlocEventLogin){
      print("cekk");
      yield* _login(event.myData);
    }
    if(event is BlocCekToken){
      yield* _cekToken();
    }
  }
}

Stream<LoginState> _cekToken()async*{
  print("sini");
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield BlocStateLoading();
  try {
    String? token = await preferences.getString("token");
    if(token != null){
      dynamic value = await LoginApi.cekTokenLogin(token);
      var hasil = jsonDecode(value);
      ModelResponseLogin hasilConvert = ModelResponseLogin.fromJson(hasil);
      yield BlocStateCekTokenSukses(data: hasilConvert);
    } else {
      yield BlocaStateCekTokenFailed();
    }
  } catch (e) {
    print(e.toString());
    yield BlocaStateCekTokenFailed();
  }
}

Stream<LoginState> _login(data)async*{
  print(data);
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield BlocStateLoading();
  try {
    print("masuk");
      dynamic value = await LoginApi.loginAkun(data);
      var hasil = jsonDecode(value);
      ModelResponseLogin hasilConvert = ModelResponseLogin.fromJson(hasil);
      await preferences.setString("email", hasilConvert.email!);
      await preferences.setString("token", hasilConvert.token!);
      await preferences.setString("myname", hasilConvert.nama!);
      await preferences.setInt("role", hasilConvert.role!);
      await preferences.setInt("id_pengguna", hasilConvert.id_pengguna!);
      await preferences.setInt("status", hasilConvert.status!);
      // streamSocket;
    yield BlocStateSukses(myData: hasilConvert);
  } catch (e) {
    yield BlocaStateFailed(errorMessage: "gagal login");
  }
}


class StreamSocket{
  final _socketResponse= StreamController<String>();
  void Function(String) get addResponse => _socketResponse.sink.add;

  Stream<String> get getResponse => _socketResponse.stream;


  void dispose(){
    _socketResponse.close();
  }
}

StreamSocket streamSocket =StreamSocket();

//STEP2: Add this function in main function in main.dart file and add incoming data to the stream
// void connectAndListen(){
//   print("cek2");
//   IO.Socket socket = SocketLanChat()

//     socket.onConnect((_) {
//       // print(haner);
//      print('connect');
//      socket.emit('new user1', {
//        'id':1,'nama':'coba'
//      });
//     });

//     //When an event recieved from server, data is added to the stream
//     // socket.on('event', (data) => streamSocket.addResponse);
//     socket.onDisconnect((_) => print('disconnect'));

// }

void disconnect(){
  print("cek");
  // IO.Socket socket = IO.io('http://103.122.97.232:4000/',
  //     OptionBuilder()
  //      .setTransports(['websocket']).build());

  //   // socket.disconnect();
  //   socket.onDisconnect((_) => print('disconnect'));

}