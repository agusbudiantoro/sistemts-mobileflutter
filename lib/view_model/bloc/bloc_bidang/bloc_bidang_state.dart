part of 'bloc_bidang_bloc.dart';

@immutable
abstract class BlocBidangState {}

class BlocBidangInitial extends BlocBidangState {}

class BlocGetBidangSuccessState extends BlocBidangState {
  final List<ModelListBidang>? listData;
  BlocGetBidangSuccessState({this.listData}); 
}

class BlocGetBidangLoadingState extends BlocBidangState {
}

class BlocGetBidangFailedState extends BlocBidangState {
  final String? errorMessage;
  BlocGetBidangFailedState({this.errorMessage});
}