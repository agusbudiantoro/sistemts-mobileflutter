import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sistemts/model/model_list_bidang/model_list_bidang.dart';
import 'package:sistemts/view_model/api/bidang/api_bidang.dart';

part 'bloc_bidang_event.dart';
part 'bloc_bidang_state.dart';

class BlocBidangBloc extends Bloc<BlocBidangEvent, BlocBidangState> {
  BlocBidangBloc() : super(BlocBidangInitial());

  @override
  Stream<BlocBidangState> mapEventToState(
    BlocBidangEvent event,
  ) async* {
    if(event is EventGetBidang){
      print("cek");
      yield* _getBidang();
    }
  }
}

Stream<BlocBidangState> _getBidang()async*{
  yield BlocGetBidangLoadingState();
  try {
    dynamic data = await BidangApi.getBidang();
    // print(data);
    var listData = jsonDecode(data) as List;
    // print(listData);
    var hasilFinal = listData.map<ModelListBidang>((item) => ModelListBidang.fromJson(item)).toList();
    List<ModelListBidang> hasilConvert=hasilFinal;
    yield BlocGetBidangSuccessState(listData: hasilConvert); 
  } catch (e) {
    BlocGetBidangFailedState(errorMessage: e.toString());
  }
}